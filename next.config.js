/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: '**',
            },
        ],
    },
    env: {
        URL: process.env.NEXT_PUBLIC_URL,
    }
}

module.exports = nextConfig;
