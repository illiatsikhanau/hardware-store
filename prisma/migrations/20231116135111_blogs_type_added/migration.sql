-- AlterTable
ALTER TABLE "Blog" ADD COLUMN     "typeId" INTEGER;

-- AddForeignKey
ALTER TABLE "Blog" ADD CONSTRAINT "Blog_typeId_fkey" FOREIGN KEY ("typeId") REFERENCES "BlogType"("id") ON DELETE SET NULL ON UPDATE CASCADE;
