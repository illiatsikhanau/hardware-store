/*
  Warnings:

  - Made the column `chipColor` on table `Promotion` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Promotion" ALTER COLUMN "chipColor" SET NOT NULL;
