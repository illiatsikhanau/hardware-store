-- CreateTable
CREATE TABLE "Banner" (
    "id" SERIAL NOT NULL,
    "src" TEXT NOT NULL,
    "href" TEXT,

    CONSTRAINT "Banner_pkey" PRIMARY KEY ("id")
);
