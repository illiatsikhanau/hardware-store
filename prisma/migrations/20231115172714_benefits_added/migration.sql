-- CreateTable
CREATE TABLE "Benefit" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "icon" TEXT NOT NULL,
    "href" TEXT,

    CONSTRAINT "Benefit_pkey" PRIMARY KEY ("id")
);
