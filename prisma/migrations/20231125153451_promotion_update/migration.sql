/*
  Warnings:

  - Added the required column `every` to the `Promotion` table without a default value. This is not possible if the table is not empty.
  - Added the required column `from` to the `Promotion` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Promotion" ADD COLUMN     "every" INTEGER NOT NULL,
ADD COLUMN     "from" INTEGER NOT NULL,
ADD COLUMN     "startsAt" TIMESTAMP(3);
