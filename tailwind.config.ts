import type {Config} from 'tailwindcss';
import {nextui} from '@nextui-org/theme';

const config: Config = {
    content: [
        './src/components/**/*.{js,ts,jsx,tsx,mdx}',
        './src/app/**/*.{js,ts,jsx,tsx,mdx}',
        './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
    ],
    theme: {
        extend: {
            screens: {
                'xs': '360px',
                's': '480px',
            },
            spacing: {
                '18': '4.5rem',
                '30': '7.5rem',
            },
            boxShadow: {
                card: '0 0 16px 0 rgba(0, 0, 0, 0.3)',
            },
            colors: {
                'main': '#af5959',
                'accent': '#3b5e83',
                'green': '#228572',
                'red': '#b91c1c',

                'tiktok': '#333333',
                'youtube': '#b62d28',
                'telegram': '#2190c8',
                'whatsapp': '#1b9646',
                'viber': '#724b92',
                'vk': '#456e9c',
            },
            zIndex: {
                '1': '1',
                '2': '2',
            },
        },
    },
    safelist: [
        'bg-[#000000]', 'bg-[#3f3f46]', 'bg-[#3b5e83]', 'bg-[#0ea5e9]', 'bg-[#228572]', 'bg-[#af5959]', 'bg-[#b91c1c]',
    ],
    darkMode: 'class',
    plugins: [nextui()],
}
export default config
