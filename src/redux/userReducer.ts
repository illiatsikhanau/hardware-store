import {createSlice, PayloadAction} from '@reduxjs/toolkit';

interface UserState {
    favoriteCount: number
}

export const initialState: UserState = {
    favoriteCount: 0,
}

export const userSlice = createSlice({
    name: 'admin',
    initialState,
    reducers: {
        setFavoriteCount(state, action: PayloadAction<number>) {
            state.favoriteCount = action.payload;
        },
        incFavoriteCount(state) {
            state.favoriteCount++;
        },
        decFavoriteCount(state) {
            state.favoriteCount--;
        },
    }
});

export const {
    setFavoriteCount, incFavoriteCount, decFavoriteCount,
} = userSlice.actions;

export default userSlice.reducer;
