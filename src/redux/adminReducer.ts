import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {CategoryExt, EMPTY_CATEGORY} from '@/services/categories';
import {SubcategoryExt, EMPTY_SUBCATEGORY} from '@/services/subcategories';
import {SectionExt, EMPTY_SECTION} from '@/services/sections';
import {BannerExt, EMPTY_BANNER} from '@/services/banners';
import {BenefitExt, EMPTY_BENEFIT} from '@/services/benefits';
import {BlogTypeExt, EMPTY_BLOG_TYPE} from '@/services/blogTypes';
import {BlogExt, EMPTY_BLOG} from '@/services/blogs';
import {ProductUnitExt, EMPTY_PRODUCT_UNIT} from '@/services/productUnits';
import {ProductExt, EMPTY_PRODUCT} from '@/services/products';
import {PromotionExt, EMPTY_PROMOTION} from '@/services/promotions';

interface AdminState {
    activeCategory: CategoryExt
    activeSubcategory: SubcategoryExt
    activeSection: SectionExt
    activeBanner: BannerExt
    activeBenefit: BenefitExt
    activeBlogType: BlogTypeExt
    activeBlog: BlogExt
    activeProductUnit: ProductUnitExt
    activeProduct: ProductExt
    activePromotion: PromotionExt
}

export const initialState: AdminState = {
    activeCategory: EMPTY_CATEGORY,
    activeSubcategory: EMPTY_SUBCATEGORY,
    activeSection: EMPTY_SECTION,
    activeBanner: EMPTY_BANNER,
    activeBenefit: EMPTY_BENEFIT,
    activeBlogType: EMPTY_BLOG_TYPE,
    activeBlog: EMPTY_BLOG,
    activeProductUnit: EMPTY_PRODUCT_UNIT,
    activeProduct: EMPTY_PRODUCT,
    activePromotion: EMPTY_PROMOTION,
}

export const adminSlice = createSlice({
    name: 'admin',
    initialState,
    reducers: {
        setActiveCategory(state, action: PayloadAction<CategoryExt>) {
            state.activeCategory = action.payload;
        },
        updateActiveCategory(state, action: PayloadAction<any[]>) {
            state.activeCategory = {
                ...state.activeCategory,
                [action.payload[0]]: action.payload[1]
            }
        },
        setActiveSubcategory(state, action: PayloadAction<SubcategoryExt>) {
            state.activeSubcategory = {
                ...action.payload,
                categoryId: state.activeCategory.id
            };
        },
        updateActiveSubcategory(state, action: PayloadAction<any[]>) {
            state.activeSubcategory = {
                ...state.activeSubcategory,
                [action.payload[0]]: action.payload[1],
                categoryId: state.activeCategory.id
            }
        },
        setActiveSection(state, action: PayloadAction<SectionExt>) {
            state.activeSection = {
                ...action.payload,
                subcategoryId: state.activeSubcategory.id
            };
        },
        updateActiveSection(state, action: PayloadAction<any[]>) {
            state.activeSection = {
                ...state.activeSection,
                [action.payload[0]]: action.payload[1],
                subcategoryId: state.activeSubcategory.id
            }
        },
        setActiveBanner(state, action: PayloadAction<BannerExt>) {
            state.activeBanner = action.payload;
        },
        updateActiveBanner(state, action: PayloadAction<any[]>) {
            state.activeBanner = {
                ...state.activeBanner,
                [action.payload[0]]: action.payload[1]
            }
        },
        setActiveBenefit(state, action: PayloadAction<BenefitExt>) {
            state.activeBenefit = action.payload;
        },
        updateActiveBenefit(state, action: PayloadAction<any[]>) {
            state.activeBenefit = {
                ...state.activeBenefit,
                [action.payload[0]]: action.payload[1]
            }
        },
        setActiveBlogType(state, action: PayloadAction<BlogTypeExt>) {
            state.activeBlogType = action.payload;
        },
        updateActiveBlogType(state, action: PayloadAction<any[]>) {
            state.activeBlogType = {
                ...state.activeBlogType,
                [action.payload[0]]: action.payload[1]
            }
        },
        setActiveBlog(state, action: PayloadAction<BlogExt>) {
            state.activeBlog = action.payload;
        },
        updateActiveBlog(state, action: PayloadAction<any[]>) {
            state.activeBlog = {
                ...state.activeBlog,
                [action.payload[0]]: action.payload[1]
            }
        },
        setActiveProductUnit(state, action: PayloadAction<ProductUnitExt>) {
            state.activeProductUnit = action.payload;
        },
        updateActiveProductUnit(state, action: PayloadAction<any[]>) {
            state.activeProductUnit = {
                ...state.activeProductUnit,
                [action.payload[0]]: action.payload[1]
            }
        },
        setActiveProduct(state, action: PayloadAction<ProductExt>) {
            state.activeProduct = {
                ...action.payload,
                sectionId: state.activeSection.id
            };
        },
        updateActiveProduct(state, action: PayloadAction<any[]>) {
            state.activeProduct = {
                ...state.activeProduct,
                [action.payload[0]]: action.payload[1],
                sectionId: state.activeSection.id
            }
        },
        setActivePromotion(state, action: PayloadAction<PromotionExt>) {
            state.activePromotion = {
                ...action.payload,
            };
        },
        updateActivePromotion(state, action: PayloadAction<any[]>) {
            state.activePromotion = {
                ...state.activePromotion,
                [action.payload[0]]: action.payload[1],
            }
        },
    }
});

export const {
    setActiveCategory, updateActiveCategory,
    setActiveSubcategory, updateActiveSubcategory,
    setActiveSection, updateActiveSection,
    setActiveBanner, updateActiveBanner,
    setActiveBenefit, updateActiveBenefit,
    setActiveBlogType, updateActiveBlogType,
    setActiveBlog, updateActiveBlog,
    setActiveProductUnit, updateActiveProductUnit,
    setActiveProduct, updateActiveProduct,
    setActivePromotion, updateActivePromotion,
} = adminSlice.actions;

export default adminSlice.reducer;
