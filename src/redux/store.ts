import {configureStore} from '@reduxjs/toolkit';
import adminReducer from '@/redux/adminReducer';
import userReducer from '@/redux/userReducer';

const store = configureStore({
    reducer: {
        admin: adminReducer,
        user: userReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
    }),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
