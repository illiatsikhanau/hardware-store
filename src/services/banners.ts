import {Banner} from '@prisma/client';

export const BANNERS_PER_PAGE = 30;

export interface BannerExt extends Banner {
}

export const EMPTY_BANNER: BannerExt = {
    id: 0,
    src: '',
    href: null,
    expiresAt: null,
}

export interface BannersQuery {
    onlyActive?: boolean
    skip?: number
    take?: number
}

export const createBanner = async (banner: BannerExt): Promise<BannerExt | null> => {
    const result = await fetch(`${process.env.URL}/api/banners`, {
        method: 'POST',
        body: JSON.stringify(banner),
    });
    return result.ok ? result.json() : null;
}

export const getBanners = async (query?: BannersQuery): Promise<Banner[]> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/banners?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const getBannersInfo = async (query?: BannersQuery): Promise<any> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/banners/info?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : null;
}

export const updateBanner = async (banner: BannerExt): Promise<BannerExt | null> => {
    const result = await fetch(`${process.env.URL}/api/banners/${banner.id}`, {
        method: 'PUT',
        body: JSON.stringify(banner),
    });
    return result.ok ? result.json() : null;
}

export const deleteBanner = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/banners/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
