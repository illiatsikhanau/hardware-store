import {Blog} from '@prisma/client';
import {BlogTypeExt} from '@/services/blogTypes';

export const BLOGS_PER_PAGE = 30;

export interface BlogExt extends Blog {
    type: BlogTypeExt | null
}

export const EMPTY_BLOG: BlogExt = {
    id: 0,
    name: '',
    src: '',
    href: '',
    createdAt: null,
    typeId: null,
    type: null
}

export interface BlogsQuery {
    name?: string
    skip?: number
    take?: number
}

export const createBlog = async (blog: BlogExt): Promise<BlogExt | null> => {
    const result = await fetch(`${process.env.URL}/api/blogs`, {
        method: 'POST',
        body: JSON.stringify(blog),
    });
    return result.ok ? result.json() : null;
}

export const getBlogs = async (query?: BlogsQuery): Promise<BlogExt[]> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/blogs?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const getBlogsInfo = async (query?: BlogsQuery): Promise<any> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/blogs/info?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : null;
}

export const updateBlog = async (blog: BlogExt): Promise<BlogExt | null> => {
    const result = await fetch(`${process.env.URL}/api/blogs/${blog.id}`, {
        method: 'PUT',
        body: JSON.stringify(blog),
    });
    return result.ok ? result.json() : null;
}

export const deleteBlog = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/blogs/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
