export const addFavorite = async (productId: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/favorite`, {
        method: 'POST',
        body: JSON.stringify({productId: productId}),
    });
    return result.ok;
}

export const deleteFavorite = async (productId: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/favorite/${productId}`, {
        method: 'DELETE'
    });
    return result.ok;
}
