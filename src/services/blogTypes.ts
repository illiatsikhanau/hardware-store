import {BlogType} from '@prisma/client';

export interface BlogTypeExt extends BlogType {
}

export const EMPTY_BLOG_TYPE: BlogTypeExt = {
    id: 0,
    name: '',
}

export const createBlogType = async (benefit: BlogTypeExt): Promise<BlogTypeExt | null> => {
    const result = await fetch(`${process.env.URL}/api/blog-types`, {
        method: 'POST',
        body: JSON.stringify(benefit),
    });
    return result.ok ? result.json() : null;
}

export const getBlogTypes = async (): Promise<BlogTypeExt[]> => {
    const result = await fetch(`${process.env.URL}/api/blog-types`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const updateBlogType = async (benefit: BlogTypeExt): Promise<BlogTypeExt | null> => {
    const result = await fetch(`${process.env.URL}/api/blog-types/${benefit.id}`, {
        method: 'PUT',
        body: JSON.stringify(benefit),
    });
    return result.ok ? result.json() : null;
}

export const deleteBlogType = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/blog-types/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
