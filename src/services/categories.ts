import {Category} from '@prisma/client';

export interface CategoryExt extends Category {
}

export const EMPTY_CATEGORY: CategoryExt = {
    id: 0,
    name: '',
    src: '',
    icon: '',
}

export const createCategory = async (category: CategoryExt): Promise<CategoryExt | null> => {
    const result = await fetch(`${process.env.URL}/api/categories`, {
        method: 'POST',
        body: JSON.stringify(category),
    });
    return result.ok ? result.json() : null;
}

export const getCategories = async (): Promise<CategoryExt[]> => {
    const result = await fetch(`${process.env.URL}/api/categories`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const updateCategory = async (category: CategoryExt): Promise<CategoryExt | null> => {
    const result = await fetch(`${process.env.URL}/api/categories/${category.id}`, {
        method: 'PUT',
        body: JSON.stringify(category),
    });
    return result.ok ? result.json() : null;
}

export const deleteCategory = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/categories/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
