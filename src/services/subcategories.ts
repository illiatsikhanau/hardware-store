import {Subcategory} from '@prisma/client';

export interface SubcategoryExt extends Subcategory {
}

export const EMPTY_SUBCATEGORY: SubcategoryExt = {
    id: 0,
    name: '',
    categoryId: 0,
}

export const createSubcategory = async (subcategory: SubcategoryExt): Promise<SubcategoryExt | null> => {
    const result = await fetch(`${process.env.URL}/api/subcategories`, {
        method: 'POST',
        body: JSON.stringify(subcategory),
    });
    return result.ok ? result.json() : null;
}

export const getSubcategories = async (query?: { categoryId?: string }): Promise<SubcategoryExt[]> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/subcategories?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const updateSubcategory = async (subcategory: SubcategoryExt): Promise<SubcategoryExt | null> => {
    const result = await fetch(`${process.env.URL}/api/subcategories/${subcategory.id}`, {
        method: 'PUT',
        body: JSON.stringify(subcategory),
    });
    return result.ok ? result.json() : null;
}

export const deleteSubcategory = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/subcategories/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
