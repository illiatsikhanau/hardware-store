import {User} from '@prisma/client';

export const USERS_PER_PAGE = 30;

export interface UserExt extends User {
}

export interface UsersQuery {
    email?: string
    skip?: number
    take?: number
}

export const getUsers = async (query?: UsersQuery): Promise<User[]> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/users?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const getUsersInfo = async (query?: UsersQuery): Promise<any> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/users/info?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : null;
}

export const updateUser = async (user: User): Promise<User | null> => {
    const result = await fetch(`${process.env.URL}/api/users/${user.id}`, {
        method: 'PUT',
        body: JSON.stringify(user),
    });
    return result.ok ? result.json() : null;
}
