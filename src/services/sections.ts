import {Section} from '@prisma/client';

export interface SectionExt extends Section {
}

export const EMPTY_SECTION: SectionExt = {
    id: 0,
    name: '',
    subcategoryId: 0,
}

export const createSection = async (section: SectionExt): Promise<SectionExt | null> => {
    const result = await fetch(`${process.env.URL}/api/sections`, {
        method: 'POST',
        body: JSON.stringify(section),
    });
    return result.ok ? result.json() : null;
}

export const getSections = async (query?: { subcategoryId?: string }): Promise<SectionExt[]> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/sections?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const getSection = async (id: number): Promise<SectionExt | null> => {
    const result = await fetch(`${process.env.URL}/api/sections/${id}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : null;
}

export const updateSection = async (section: SectionExt): Promise<SectionExt | null> => {
    const result = await fetch(`${process.env.URL}/api/sections/${section.id}`, {
        method: 'PUT',
        body: JSON.stringify(section),
    });
    return result.ok ? result.json() : null;
}

export const deleteSection = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/sections/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
