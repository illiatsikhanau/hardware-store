export const uploadFile = async (file: File): Promise<string | null> => {
    const formData = new FormData();
    formData.append('file', file);

    const result = await fetch(`${process.env.URL}/api/upload`, {
        method: 'POST',
        body: formData,
    });
    return result.ok ? result.json() : null;
}
