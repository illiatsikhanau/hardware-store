import {Product} from '@prisma/client';
import {ProductUnitExt} from '@/services/productUnits';

export const PRODUCTS_PER_PAGE = 30;

interface ProductPromotion {
    id: number,
    chipName: string,
    chipColor: number,
}

export interface ProductExt extends Product {
    unit: ProductUnitExt | null
    promotions: ProductPromotion[]
    isFavorite: boolean
}

export const EMPTY_PRODUCT: ProductExt = {
    id: 0,
    name: '',
    src: '',
    count: 0,
    price: 0,
    unitId: null,
    unit: null,
    sectionId: 0,
    promotions: [],
    isFavorite: false,
}

export interface ProductsQuery {
    sort?: string
    name?: string
    count?: string
    price?: string
    sectionId?: string
    onlyFavorite?: boolean
    skip?: number
    take?: number
}

export const createProduct = async (product: ProductExt): Promise<ProductExt | null> => {
    const result = await fetch(`${process.env.URL}/api/products`, {
        method: 'POST',
        body: JSON.stringify(product),
    });
    return result.ok ? result.json() : null;
}

export const getProducts = async (query?: ProductsQuery): Promise<ProductExt[]> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/products?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const getProductsInfo = async (query?: ProductsQuery): Promise<any> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/products/info?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : null;
}

export const updateProduct = async (product: ProductExt): Promise<ProductExt | null> => {
    const result = await fetch(`${process.env.URL}/api/products/${product.id}`, {
        method: 'PUT',
        body: JSON.stringify(product),
    });
    return result.ok ? result.json() : null;
}

export const deleteProduct = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/products/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
