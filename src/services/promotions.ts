import {Promotion} from '@prisma/client';

export const PROMOTIONS_PER_PAGE = 30;

interface PromotionProductId {
    id: number
}

export interface PromotionExt extends Promotion {
    products: PromotionProductId[]
}

export const EMPTY_PROMOTION: PromotionExt = {
    id: 0,
    name: '',
    chipName: '',
    chipColor: 0,
    discount: 0,
    from: 1,
    every: 1,
    startsAt: null,
    expiresAt: null,
    products: []
}

export interface PromotionsQuery {
    name?: string
    skip?: number
    take?: number
}

export const createPromotion = async (promotion: PromotionExt): Promise<PromotionExt | null> => {
    const result = await fetch(`${process.env.URL}/api/promotions`, {
        method: 'POST',
        body: JSON.stringify(promotion),
    });
    return result.ok ? result.json() : null;
}

export const getPromotions = async (query?: PromotionsQuery): Promise<PromotionExt[]> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/promotions?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const getPromotionsInfo = async (query?: PromotionsQuery): Promise<any> => {
    const filter = query ? Object.entries(query).map(arr => arr.join('=')).join('&') : '';
    const result = await fetch(`${process.env.URL}/api/promotions/info?${filter}`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : null;
}

export const updatePromotion = async (promotion: PromotionExt): Promise<PromotionExt | null> => {
    const result = await fetch(`${process.env.URL}/api/promotions/${promotion.id}`, {
        method: 'PUT',
        body: JSON.stringify(promotion),
    });
    return result.ok ? result.json() : null;
}

export const deletePromotion = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/promotions/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
