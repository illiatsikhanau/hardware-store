import {ProductUnit} from '@prisma/client';

export interface ProductUnitExt extends ProductUnit {
}

export const EMPTY_PRODUCT_UNIT: ProductUnitExt = {
    id: 0,
    name: '',
}

export const createProductUnit = async (productUnit: ProductUnitExt): Promise<ProductUnitExt | null> => {
    const result = await fetch(`${process.env.URL}/api/product-units`, {
        method: 'POST',
        body: JSON.stringify(productUnit),
    });
    return result.ok ? result.json() : null;
}

export const getProductUnits = async (): Promise<ProductUnitExt[]> => {
    const result = await fetch(`${process.env.URL}/api/product-units`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const updateProductUnit = async (productUnit: ProductUnitExt): Promise<ProductUnitExt | null> => {
    const result = await fetch(`${process.env.URL}/api/product-units/${productUnit.id}`, {
        method: 'PUT',
        body: JSON.stringify(productUnit),
    });
    return result.ok ? result.json() : null;
}

export const deleteProductUnit = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/product-units/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
