import {Benefit} from '@prisma/client';

export interface BenefitExt extends Benefit {
}

export const EMPTY_BENEFIT: BenefitExt = {
    id: 0,
    name: '',
    icon: '',
    href: null
}

export const createBenefit = async (benefit: BenefitExt): Promise<BenefitExt | null> => {
    const result = await fetch(`${process.env.URL}/api/benefits`, {
        method: 'POST',
        body: JSON.stringify(benefit),
    });
    return result.ok ? result.json() : null;
}

export const getBenefits = async (): Promise<BenefitExt[]> => {
    const result = await fetch(`${process.env.URL}/api/benefits`, {
        method: 'GET',
        cache: 'no-store',
    });
    return result.ok ? result.json() : [];
}

export const updateBenefit = async (benefit: BenefitExt): Promise<BenefitExt | null> => {
    const result = await fetch(`${process.env.URL}/api/benefits/${benefit.id}`, {
        method: 'PUT',
        body: JSON.stringify(benefit),
    });
    return result.ok ? result.json() : null;
}

export const deleteBenefit = async (id: number): Promise<boolean> => {
    const result = await fetch(`${process.env.URL}/api/benefits/${id}`, {
        method: 'DELETE'
    });
    return result.ok;
}
