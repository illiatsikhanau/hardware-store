import moment from 'moment';

export const getDateString = (date: Date | null) => date ? moment(date).format('DD.MM.YYYY') : '';
export const isEqualDates = (date1: Date | null, date2: Date | null) => moment(date1).diff(date2, 'days') !== 0;
