import {AppRouterInstance} from 'next/dist/shared/lib/app-router-context.shared-runtime';
import {ReadonlyURLSearchParams} from 'next/navigation';

interface Param {
    name: string
    value: string
}

export const updateSearchParams = (
    router: AppRouterInstance,
    pathname: string,
    searchParams: ReadonlyURLSearchParams,
    newParams: Param[]
) => {
    const params = new URLSearchParams(Array.from(searchParams.entries()));
    newParams.forEach(param => param.value ?
        params.set(param.name, param.value) :
        params.delete(param.name)
    );
    router.push(`${pathname}?${params.toString()}`);
}
