const observerOptions = {
    root: null,
    rootMargin: '0px',
    threshold: 1
}

export default (element: Element, onVisible: () => void) => {
    const observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                onVisible();
                observer.disconnect();
            }
        })
    }, observerOptions);
    observer.observe(element);
    return observer;
}
