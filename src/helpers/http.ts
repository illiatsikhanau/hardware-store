import {NextResponse} from 'next/server';

export const ok = NextResponse.json('Ok', {status: 200});
export const badRequest = NextResponse.json('Bad Request', {status: 400});
export const forbidden = NextResponse.json('Forbidden', {status: 403});
export const serviceUnavailable = NextResponse.json('Service Unavailable', {status: 503});
