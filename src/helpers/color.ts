export const getHexColor = (color: number) => `#${color.toString(16).padStart(6, '0')}`;
export const COLORS = ['#000000', '#3f3f46', '#3b5e83', '#0ea5e9', '#228572', '#af5959', '#b91c1c'];
