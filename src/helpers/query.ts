import {NextRequest} from 'next/server';

const ALLOWED_OPERATORS = ['equals', 'in', 'notIn', 'lt', 'lte', 'gt', 'gte', 'not', 'contains'];

export const getSortCondition = (req: NextRequest) => {
    const param = req.nextUrl.searchParams.get('sort');
    const paramCondition = param?.split(',')
        .map(p => p.split(':'))
        .filter(p => p.length === 2);
    return paramCondition?.reduce((obj, p) => ({
        ...obj,
        [p[0]]: p[1]
    }), {});
}

export const getObjectParamCondition = (req: NextRequest, paramName: string, isFloat = false) => {
    const param = req.nextUrl.searchParams.get(paramName);
    const paramCondition = param?.split(',')
        .map(p => p.split(':'))
        .filter(p =>
            p.length === 2 &&
            ALLOWED_OPERATORS.includes(p[0]) &&
            !isFloat || !isNaN(Number(p[1]))
        );
    return paramCondition?.reduce((obj, p) => ({
        ...obj,
        [p[0]]: isFloat ? Number(p[1]) : p[1]
    }), {});
}
