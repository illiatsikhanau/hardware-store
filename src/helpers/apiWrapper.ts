import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import {Role} from '@prisma/client';
import {NextResponse} from 'next/server';
import {forbidden, serviceUnavailable} from '@/helpers/http';

export const withApiWrapper = async (access: Role[], callback: (session: any) => Promise<NextResponse>) => {
    const session = await getServerSession(authOptions);
    if (access.length) {
        if (access.includes(session.user.role)) {
            return await callback(session).catch(() => serviceUnavailable);
        } else {
            return forbidden;
        }
    } else {
        return await callback(session).catch(() => serviceUnavailable);
    }
}
