'use client'

import {ReactNode} from 'react';
import {Provider} from 'react-redux';
import store from '@/redux/store';
import {NextUIProvider} from '@nextui-org/system';
import {SessionProvider} from 'next-auth/react';
import App from '@/components/App';

const AppProvider = ({children}: { children: ReactNode }) => {
    return (
        <Provider store={store}>
            <NextUIProvider>
                <SessionProvider>
                    <App children={children}/>
                </SessionProvider>
            </NextUIProvider>
        </Provider>
    );
}

export default AppProvider;
