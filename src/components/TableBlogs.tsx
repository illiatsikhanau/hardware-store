'use client'

import {useEffect, useState, KeyboardEvent} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveBlog, updateActiveBlog} from '@/redux/adminReducer';
import {
    getBlogs, getBlogsInfo, createBlog, updateBlog, deleteBlog,
    BlogExt, EMPTY_BLOG, BLOGS_PER_PAGE,
} from '@/services/blogs';
import {BlogTypeExt} from '@/services/blogTypes';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import {Select, SelectItem} from '@nextui-org/select';
import {Tooltip} from '@nextui-org/tooltip';
import Image from 'next/image';
import Button from '@/components/Button';
import Spinner from '@/components/Spinner';
import Input from '@/components/Input';
import FileUpload from '@/components/FileUpload';
import Pagination from '@/components/Pagination';
import TableSearch from '@/components/TableSearch';

const helpStyle = 'w-5 h-5 flex items-center justify-center rounded-full bg-zinc-700 text-white cursor-pointer';

const TableBlogs = ({blogTypes}: { blogTypes: BlogTypeExt[] }) => {
    const dispatch = useAppDispatch();
    const activeBlog = useAppSelector(state => state.admin.activeBlog);

    const [blogs, setBlogs] = useState<BlogExt[]>();
    const [page, setPage] = useState(1);
    const [pages, setPages] = useState(1);
    const [search, setSearch] = useState('');

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitBlog = async () => {
        if (activeBlog.name && activeBlog.src && activeBlog.href) {
            const newBlog = activeBlog.id ?
                await updateBlog(activeBlog) :
                await createBlog(activeBlog);

            if (blogs && newBlog) {
                await updateBlogs().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitBlog();
        }
    }

    const removeBlog = async () => {
        const success = await deleteBlog(activeBlog.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateBlogs().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectBlog = (blog: BlogExt) => {
        dispatch(setActiveBlog(blog));
        onOpen();
    }

    const updateBlogs = async () => {
        const query = {
            name: `contains:${search}`,
            skip: (page - 1) * BLOGS_PER_PAGE,
        }

        const info = await getBlogsInfo(query);
        const blogs = await getBlogs(query);
        setPages(Math.ceil(info.count / BLOGS_PER_PAGE));
        setBlogs(blogs);
    }

    useEffect(() => {
        void updateBlogs();
    }, [page]);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveBlog(EMPTY_BLOG));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='w-full flex flex-col gap-2'>
            {blogs ?
                <>
                    <div className='flex max-lg:flex-col items-center justify-between gap-y-2'>
                        <div className='flex gap-2'>
                            <TableSearch
                                search={search}
                                setSearch={setSearch}
                                onSearch={updateBlogs}
                            />
                            <Button
                                className='min-w-unit-24 bg-green'
                                onClick={onOpen}
                                children='Добавить'
                            />
                        </div>
                        {(pages > 1 || page > 1) &&
                            <Pagination total={pages} page={page} onChange={page => setPage(page)}/>
                        }
                    </div>
                    <Table aria-label='Статьи' removeWrapper hideHeader={!blogs.length}>
                        <TableHeader>
                            <TableColumn
                                className='!rounded-l-md bg-zinc-800 text-white'
                                children='Название'
                            />
                            <TableColumn
                                className='!rounded-r-md bg-zinc-800 text-white'
                                children='Картинка'
                            />
                        </TableHeader>
                        <TableBody emptyContent='Нет статей'>
                            {blogs.map(blog =>
                                <TableRow
                                    key={blog.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectBlog(blog)}
                                >
                                    <TableCell
                                        className='rounded-l-md'
                                        children={blog.name}
                                    />
                                    <TableCell className='rounded-r-md'>
                                        <Image src={blog.src} alt='Изображение' width={84} height={56}/>
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeBlog.id ? 'Редактирование' : 'Добавление'} статьи
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название статьи'
                            value={activeBlog.name}
                            onChange={e => dispatch(updateActiveBlog(['name', e.target.value]))}
                            autoFocus
                        />
                        <div className='flex gap-2'>
                            <FileUpload
                                content='Выберите картинку'
                                defaultContent={activeBlog.src ? 'Текущая картинка' : null}
                                onChangeFileUrl={url => dispatch(updateActiveBlog(['src', url]))}
                            />
                            <div className='hidden md:flex items-center'>
                                <Tooltip placement='top' content='Рекомендуемый формат - jpg/webp 300x200 пикселей'>
                                    <span className={helpStyle} children='?'/>
                                </Tooltip>
                            </div>
                        </div>
                        <Input
                            classNames={{input: '!p-0'}}
                            type='url'
                            startContent={
                                <span className='text-sm text-zinc-400 whitespace-nowrap' children={process.env.URL}/>
                            }
                            value={activeBlog.href ?? ''}
                            onChange={e => dispatch(updateActiveBlog(['href', e.target.value.trim()]))}
                        />
                        <Select
                            classNames={{
                                trigger: 'h-10 rounded-md border-1 border-zinc-200 bg-zinc-100 !duration-100',
                                selectorIcon: 'stroke-zinc-700',
                                value: '!text-zinc-700',
                            }}
                            listboxProps={{itemClasses: {base: 'rounded-md'}}}
                            popoverProps={{classNames: {content: 'p-1 rounded-md'}}}
                            placeholder='Выберите тип статьи'
                            selectedKeys={activeBlog.typeId ? [activeBlog.typeId.toString()] : []}
                            onChange={e => dispatch(updateActiveBlog(
                                ['typeId', e.target.value ? Number(e.target.value) : null]
                            ))}
                            aria-label='Тип статьи'
                        >
                            {blogTypes.map(blogType =>
                                <SelectItem key={blogType.id} children={blogType.name}/>
                            )}
                        </Select>
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeBlog.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить статью "${activeBlog.name}"?`}
                                    onConfirm={removeBlog}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitBlog}
                            isDisabled={!activeBlog.name || !activeBlog.src || !activeBlog.href}
                            children={activeBlog.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableBlogs;
