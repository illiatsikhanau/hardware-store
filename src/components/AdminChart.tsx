'use client'

import React, {useEffect, useRef} from 'react';
import Chart, {ChartConfiguration, registerables} from 'chart.js/auto';

const LineChart: React.FC = () => {
    Chart.register(...registerables);
    const chartRef = useRef<HTMLCanvasElement>(null);
    const chartInstanceRef = useRef<Chart | null>(null);

    useEffect(() => {
        if (chartRef.current) {
            const myChartRef = chartRef.current.getContext('2d');
            if (myChartRef) {
                if (chartInstanceRef.current) {
                    chartInstanceRef.current.destroy();
                }
                chartInstanceRef.current = new Chart(myChartRef, getConfig());
            }
        }
    }, []);

    const getConfig = (): ChartConfiguration<'line'> => {
        return {
            type: 'line',
            data: {
                labels: ['Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь'],
                datasets: [{
                    label: 'График продаж',
                    data: [63655, 56219, 66720, 71321, 62756, 71553, 90640],
                    fill: true,
                    borderColor: '#3b5e83',
                    tension: 0.2,
                }],
            },
        };
    };

    return <canvas ref={chartRef}/>;
};

export default LineChart;
