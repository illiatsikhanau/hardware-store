'use client'

import Link from 'next/link';
import TabSearch from '@/components/svg/TabSearchIcon';
import TabHomeIcon from '@/components/svg/TabHomeIcon';
import TabHeartIcon from '@/components/svg/TabHeartIcon';
import TabCartIcon from '@/components/svg/TabCartIcon';
import TabProfileIcon from '@/components/svg/TabProfileIcon';
import {usePathname} from 'next/navigation';

const wrapperStyle = [
    'fixed bottom-0 flex lg:hidden w-full justify-center',
    'border-t-1 border-zinc-200 backdrop-blur-md bg-white/70 z-30',
].join(' ');
const tabLinkStyle = 'w-full flex-col items-center pb-2 duration-100 group';
const tabActiveStyle = 'active text-main bg-white';
const iconStyle = 'w-12 h-12 group-[.active]:stroke-main';

const TabBar = () => {
    const pathname = usePathname();
    const getActiveStyle = (href: string) =>
        href === '/catalog' ?
            (pathname?.startsWith(href) ? tabActiveStyle : '') :
            (pathname === href ? tabActiveStyle : '');

    return (
        <nav className={wrapperStyle}>
            <Link className={`flex ${tabLinkStyle} ${getActiveStyle('/')}`} href='/'>
                <TabHomeIcon className={iconStyle}/>
                <p className='text-xs' children='Главная'/>
            </Link>
            <Link className={`hidden xs:flex ${tabLinkStyle} ${getActiveStyle('/catalog')}`} href='/catalog'>
                <TabSearch className={iconStyle}/>
                <p className='text-xs' children='Каталог'/>
            </Link>
            <Link className={`hidden xs:flex ${tabLinkStyle} ${getActiveStyle('/favorite')}`} href='/favorite'>
                <TabHeartIcon className={iconStyle}/>
                <p className='text-xs' children='Избранное'/>
            </Link>
            <Link className={`hidden xs:flex ${tabLinkStyle} ${getActiveStyle('/cart')}`} href='/cart'>
                <TabCartIcon className={iconStyle}/>
                <p className='text-xs' children='Корзина'/>
            </Link>
            <Link className={`flex ${tabLinkStyle} ${getActiveStyle('/profile')}`} href='/profile'>
                <TabProfileIcon className={iconStyle}/>
                <p className='text-xs' children='Профиль'/>
            </Link>
        </nav>
    );
}

export default TabBar;
