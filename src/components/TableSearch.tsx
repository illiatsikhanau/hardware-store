'use client'

import {KeyboardEvent} from 'react';
import Input from '@/components/Input';
import Button from '@/components/Button';

interface TableSearchProps {
    search: string
    setSearch: (value: string) => void
    onSearch: () => void
}

const TableSearch = (props: TableSearchProps) => {
    const onSearchKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (e.key === 'Enter') {
            props.onSearch();
        }
    }

    return (
        <div className='flex'>
            <Input
                classNames={{inputWrapper: 'rounded-r-none'}}
                placeholder='Поиск'
                value={props.search}
                onChange={e => props.setSearch(e.target.value)}
                onKeyDown={onSearchKeyDown}
            />
            <Button
                className='w-fit rounded-l-none bg-transparent text-green border-2 border-green'
                onClick={props.onSearch}
                children='Поиск'
            />
        </div>
    );
}

export default TableSearch;
