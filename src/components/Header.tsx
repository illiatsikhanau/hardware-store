import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import {Role} from '@prisma/client';
import Script from 'next/script';
import Link from 'next/link';
import Avatar from '@/components/Avatar';
import Search from '@/components/Search';
import BadgeItem from '@/components/BadgeItem';
import Logo from '@/components/svg/Logo';
import PinIcon from '@/components/svg/PinIcon';

const stickyStyle = 'top-0 flex w-full justify-center border-b-1 border-zinc-200 backdrop-blur-md z-30 group/sticky';
const menuItemStyle = 'py-2 px-4 duration-100 hover:text-white hover:bg-zinc-800';

const Header = async () => {
    const session = await getServerSession(authOptions);

    const address = 'Хельсинки, Сенатская площадь';
    const workingHours = 'пн-вс: 8:00-19:00';
    const phone = '+358-0-000-000';

    return (
        <header className='flex flex-col'>
            <div className='flex justify-center border-b-1 border-zinc-200'>
                <div className='flex w-full max-w-7xl items-center justify-center xs:justify-between gap-2 py-2 px-4'>
                    <Link className='hidden xs:max-sm:flex' href='/' aria-label='Логотип'>
                        <Logo className='h-10'/>
                    </Link>
                    <Link className='hidden sm:flex items-center gap-1' href='/'>
                        <PinIcon className='w-6 h-6 stroke-zinc-700'/>
                        <div className='flex flex-col'>
                            <b className='text-sm text-accent' children={address}/>
                            <p className='text-xs text-zinc-500' children={workingHours}/>
                        </div>
                    </Link>
                    <div className='flex flex-col sm:flex-row items-center sm:gap-2'>
                        <a
                            href={`tel:${phone}`}
                            className='text-sm font-bold duration-100 hover:text-zinc-500'
                            children={phone}
                        />
                        <b
                            className='text-sm text-accent duration-100 hover:text-zinc-500 uppercase cursor-pointer'
                            children='заказать звонок'
                        />
                    </div>
                    <Link className='hidden sm:flex' href='/profile'>
                        <Avatar size='md'/>
                    </Link>
                </div>
            </div>
            <div className='flex'>
                <span className='h-14'/>
                <div id='sticky' className={stickyStyle}>
                    <div className='flex w-full items-center max-w-7xl gap-2 py-2 px-4'>
                        <Link className='hidden sm:flex' href='/' aria-label='Логотип'>
                            <Logo className='h-10'/>
                        </Link>
                        <div className='hidden group-[:not(.fixed)]/sticky:md:flex flex-col justify-center'>
                            <p className='text-sm leading-4 whitespace-nowrap'>
                                Магазин строительных<br/>материалов
                            </p>
                        </div>
                        <Search/>
                        <div className='hidden sm:flex gap-1 ml-3'>
                            <BadgeItem type='favorite'/>
                            <BadgeItem type='cart'/>
                        </div>
                        <Link className='hidden group-[.fixed]/sticky:sm:flex' href='/profile'>
                            <Avatar size='md'/>
                        </Link>
                    </div>
                </div>
            </div>
            <div className='hidden lg:flex justify-center border-b-1 border-zinc-200'>
                <ul className='flex w-full max-w-7xl justify-center text-sm uppercase'>
                    <li className='flex'>
                        <Link className={menuItemStyle} href='/' children='Главная'/>
                    </li>
                    <li className='flex'>
                        <Link className={menuItemStyle} href='/catalog' children='Каталог'/>
                    </li>
                    <li className='flex'>
                        <Link className={menuItemStyle} href='/promotions' children='Акции'/>
                    </li>
                    <li className='flex'>
                        <Link className={menuItemStyle} href='/delivery' children='Доставка'/>
                    </li>
                    <li className='flex'>
                        <Link className={menuItemStyle} href='/blog' children='Статьи'/>
                    </li>
                    <li className='flex'>
                        <Link className={menuItemStyle} href='/contacts' children='Контакты'/>
                    </li>
                    <li className='flex'>
                        <Link className={menuItemStyle} href='/about' children='О компании'/>
                    </li>
                    {session?.user?.role === Role.ADMIN &&
                        <Link className={menuItemStyle} href='/admin' children='Управление'/>
                    }
                </ul>
            </div>
            <Script src='/script.js'/>
        </header>
    );
}

export default Header;
