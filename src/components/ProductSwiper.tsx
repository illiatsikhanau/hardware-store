'use client'

import {ProductExt} from '@/services/products';
import {getHexColor} from '@/helpers/color';
import 'swiper/css';
import 'swiper/css/autoplay';
import 'swiper/css/keyboard';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import {Swiper, SwiperSlide} from 'swiper/react';
import {Autoplay, Keyboard, Pagination, Navigation} from 'swiper/modules';
import Link from 'next/link';
import Image from 'next/image';
import Chip from '@/components/Chip';

const ProductSwiper = ({products}: { products: ProductExt[] }) => {
    return (
        <div className='w-full flex lg:hidden p-1'>
            {products.length > 0 &&
                <Swiper
                    loop={true}
                    autoplay={{
                        delay: 4000,
                        disableOnInteraction: false,
                    }}
                    keyboard={true}
                    pagination={{clickable: true}}
                    navigation={true}
                    modules={[Autoplay, Keyboard, Pagination, Navigation]}
                >
                    {products.map((product, pIndex) =>
                        <SwiperSlide key={product.id}>
                            <Link
                                className='w-full flex flex-col items-center pb-4 duration-100 hover:opacity-80'
                                href='/'
                                aria-label={product.name}
                            >
                                <div className='w-full max-w-xs flex h-24 justify-between'>
                                    <div className='flex flex-col gap-2'>
                                        {product.promotions.map(promotion =>
                                            <Chip
                                                key={promotion.id}
                                                classNames={{
                                                    base: ['uppercase z-20 opacity-90', `bg-[${getHexColor(promotion.chipColor)}]`]
                                                }}
                                                children={promotion.chipName}
                                            />
                                        )}
                                    </div>
                                </div>
                                <Image
                                    className='w-full h-40 -mt-24 object-contain'
                                    src={product.src}
                                    alt='Изображение'
                                    width={300}
                                    height={150}
                                    priority={!pIndex}
                                    placeholder={!!pIndex ? 'blur' : undefined}
                                    blurDataURL={!!pIndex ? '/placeholder.svg' : undefined}
                                />
                                <p
                                    className='h-12 w-full text-center leading-6 text-zinc-700 overflow-hidden'
                                    children={product.name}
                                />
                                <p
                                    className='mt-4 px-4 py-3 rounded-md text-white bg-accent'
                                    children='Смотреть в каталоге'
                                />
                            </Link>
                        </SwiperSlide>
                    )}
                </Swiper>
            }
        </div>
    );
}

export default ProductSwiper;
