import {ProductExt} from '@/services/products';
import {getHexColor} from '@/helpers/color';
import Image from 'next/image';
import Link from 'next/link';
import TrashIcon from '@/components/svg/TrashIcon';
import Input from '@/components/Input';
import Button from '@/components/Button';
import PlusIcon from '@/components/svg/PlusIcon';
import MinusIcon from '@/components/svg/MinusIcon';
import Chip from '@/components/Chip';
import FavoriteButton from '@/components/FavoriteButton';

const counterButtonStyle = 'w-10 min-w-unit-10 h-10 p-2 text-xl bg-zinc-100 hover:bg-zinc-200';

const CartCard = ({product}: { product: ProductExt }) => {
    const imageStyle = `w-32 h-24 object-scale-down rounded-md ${product.count ? '' : 'opacity-50'}`;

    const orderCount = 1;

    return (
        <div className='flex w-full gap-3 p-2 rounded-md border-1 border-zinc-200 duration-100 hover:shadow-card'>
            <div className='flex flex-col'>
                <div className='w-32 h-24 flex flex-col gap-1'>
                    {product.promotions.map(promotion =>
                        <Chip
                            key={promotion.id}
                            classNames={{
                                base: ['uppercase z-20 opacity-90', `bg-[${getHexColor(promotion.chipColor)}]`]
                            }}
                            size='sm'
                            children={promotion.chipName}
                        />
                    )}
                </div>
                <Link className='flex -mt-24 duration-100 hover:opacity-80' href='/' aria-label={product.name}>
                    <Image
                        className={imageStyle}
                        src={product.src}
                        alt='Изображение'
                        width={128}
                        height={96}
                    />
                </Link>
                <div className='flex items-center gap-1 mt-1'>
                    <Button className={counterButtonStyle} isDisabled={!product.count} aria-label='Минус'>
                        <MinusIcon className='w-5 h-5 stroke-zinc-500'/>
                    </Button>
                    <Input
                        classNames={{
                            base: 'w-10',
                            inputWrapper: [
                                'p-0 border-0 bg-white',
                                'group-data-[focus=true]:bg-white data-[hover=true]:bg-white',
                            ],
                            input: 'text-center'
                        }}
                        type='number'
                        defaultValue={orderCount.toString()}
                        aria-label='Счетчик'
                    />
                    <Button className={counterButtonStyle} isDisabled={!product.count} aria-label='Плюс'>
                        <PlusIcon className='w-5 h-5 stroke-zinc-500'/>
                    </Button>
                </div>
            </div>
            <div className='flex flex-1 flex-col'>
                <div className='flex justify-between'>
                    {product.count ?
                        <p className='text-green'>В наличии: {product.count}</p> :
                        <p className='text-red' children='Нет в наличии'/>
                    }
                    <FavoriteButton product={product} size='sm'/>
                </div>
                <div className='flex-1'>
                    <Link
                        className='duration-100 hover:opacity-80 max-xs:break-all'
                        href='/'
                        aria-label={product.name}
                        children={product.name}
                    />
                </div>
                <p className='text-sm text-zinc-500 whitespace-nowrap'>
                    {product.price}€
                    {product.unit &&
                        <>&nbsp;/ {product.unit?.name}</>
                    }
                </p>
                <div className='flex justify-between'>
                    <b className='text-xl'>{product.price}€</b>
                    <div className='flex items-center cursor-pointer select-none group'>
                        <TrashIcon className='w-4 h-4 stroke-zinc-500 duration-100 group-hover:stroke-zinc-700'/>
                        <p className='text-zinc-500 duration-100 group-hover:text-zinc-700' children='Удалить'/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CartCard;
