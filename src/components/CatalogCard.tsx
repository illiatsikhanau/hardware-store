import {ProductExt} from '@/services/products';
import {getHexColor} from '@/helpers/color';
import Link from 'next/link';
import Image from 'next/image';
import Button from '@/components/Button';
import Input from '@/components/Input';
import Chip from '@/components/Chip';
import FavoriteButton from '@/components/FavoriteButton';
import PlusIcon from '@/components/svg/PlusIcon';
import MinusIcon from '@/components/svg/MinusIcon';

const counterButtonStyle = 'w-10 min-w-unit-10 h-10 flex flex-1 p-2 text-xl bg-zinc-100 hover:bg-zinc-200';

interface CatalogCardProps {
    product: ProductExt
    priority?: boolean
}

const CatalogCard = (props: CatalogCardProps) => {
    const orderCount = 1;

    return (
        <div className='flex w-full rounded-md border-1 border-zinc-200 duration-100 hover:shadow-card'>
            <div className='flex w-full flex-col gap-1 p-4 group'>
                <div className='flex flex-col'>
                    <div className='flex h-24 justify-between'>
                        <div className='flex flex-col gap-2'>
                            {props.product.promotions.map(promotion =>
                                <Chip
                                    key={promotion.id}
                                    classNames={{
                                        base: ['uppercase z-20 opacity-90', `bg-[${getHexColor(promotion.chipColor)}]`]
                                    }}
                                    children={promotion.chipName}
                                />
                            )}
                        </div>
                        <FavoriteButton product={props.product} size='md'/>
                    </div>
                    <Link
                        className='flex w-full h-30 justify-center -mt-24 duration-100 hover:opacity-80 select-none'
                        href='/'
                        aria-label={props.product.name}
                    >
                        <Image
                            className='object-scale-down w-full h-full'
                            src={props.product.src}
                            alt='Изображение'
                            width={200}
                            height={100}
                            priority={props.priority}
                        />
                    </Link>
                </div>
                <div className='flex h-18 flex-col'>
                    <Link
                        className='leading-6 max-xs:break-all overflow-hidden duration-100 hover:text-zinc-500'
                        href='/'
                        aria-label={props.product.name}
                        children={props.product.name}
                    />
                </div>
                <div className='flex flex-row items-center'>
                    {props.product.count ?
                        <p className='text-sm text-green'>В наличии: {props.product.count}</p> :
                        <p className='text-sm text-red' children='Нет в наличии'/>
                    }
                </div>
                <div className='flex flex-row items-center gap-1'>
                    <b className='text-xl'>{props.product.price}€</b>
                    {props.product.unit &&
                        <>
                            <span className='text-zinc-500 mb-0.5' children='/'/>
                            <span className='text-zinc-500 mb-0.5' children={props.product.unit.name}/>
                        </>
                    }
                </div>
                <div className='flex flex-row gap-1 items-center justify-between'>
                    <Button className='w-full xs:w-1/2 s:w-44' children='В корзину'/>
                    <div className='flex flex-1 gap-1'>
                        <Button className={counterButtonStyle} aria-label='Минус'>
                            <MinusIcon className='w-5 h-5 stroke-zinc-500'/>
                        </Button>
                        <Input
                            classNames={{
                                base: 'w-10',
                                inputWrapper: [
                                    'p-0 border-0 bg-white',
                                    'group-data-[focus=true]:bg-white data-[hover=true]:bg-white',
                                ],
                                input: 'text-center'
                            }}
                            type='number'
                            defaultValue={orderCount.toString()}
                            aria-label='Счетчик'
                        />
                        <Button className={counterButtonStyle} aria-label='Плюс'>
                            <PlusIcon className='w-5 h-5 stroke-zinc-500'/>
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CatalogCard;
