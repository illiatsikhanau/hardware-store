'use client'

import {ReactNode, useEffect} from 'react';
import {useAppDispatch} from '@/redux/hooks';
import {getProductsInfo} from '@/services/products';
import {setFavoriteCount} from '@/redux/userReducer';

const App = ({children}: { children: ReactNode }) => {
    const dispatch = useAppDispatch();

    useEffect(() => {
        getProductsInfo({onlyFavorite: true})
            .then(info => dispatch(setFavoriteCount(info.count)));
    }, []);

    return children;
}

export default App;
