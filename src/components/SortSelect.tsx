'use client'

import {ReactNode} from 'react';
import {usePathname, useRouter, useSearchParams} from 'next/navigation';
import {Select, SelectItem} from '@nextui-org/select';
import {updateSearchParams} from '@/helpers/searchParams';

interface SortOption {
    key: string
    value: string
    icon: ReactNode
}

interface SelectSortProps {
    selectedKey: string,
    options: SortOption[]
}

const SortSelect = (props: SelectSortProps) => {
    const pathname = usePathname();
    const searchParams = useSearchParams();
    const router = useRouter();

    const selectedIcon = props.options.find(option => option.key === props.selectedKey);

    const sort = (selectedKey: string) => {
        updateSearchParams(router, pathname, searchParams, [
            {name: 'sort', value: selectedKey}
        ]);
    }

    return (
        <Select
            classNames={{
                trigger: 'w-80 h-10 rounded-md border-1 border-zinc-200 bg-zinc-100 !duration-100',
                innerWrapper: 'gap-1',
                selectorIcon: 'stroke-zinc-700',
                value: 'font-bold !text-zinc-700',
            }}
            listboxProps={{itemClasses: {base: 'gap-1 rounded-md'}}}
            popoverProps={{classNames: {content: 'p-1 rounded-md'}}}
            startContent={selectedIcon?.icon}
            selectedKeys={[props.selectedKey]}
            onChange={e => sort(e.target.value || props.selectedKey)}
            aria-label='Сортировка'
        >
            {props.options.map(option =>
                <SelectItem
                    key={option.key}
                    startContent={option.icon}
                    children={option.value}
                />
            )}
        </Select>
    )
}

export default SortSelect;
