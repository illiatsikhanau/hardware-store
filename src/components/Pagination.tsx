import {Pagination, PaginationProps} from '@nextui-org/pagination';

const CustomPagination = (props: PaginationProps) => {
    return (
        <Pagination {...{
            ...props,
            classNames: {
                ...props.classNames,
                cursor: ['bg-zinc-800 text-white', props.classNames?.cursor]
            },
            radius: props.radius ?? 'sm',
            showControls: props.showControls ?? true,
            isCompact: props.isCompact ?? true,
        }}/>
    );
}

export default CustomPagination;
