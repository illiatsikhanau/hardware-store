'use client'

import {cloneElement, ReactElement} from 'react';
import {Modal, ModalBody, ModalContent, ModalHeader, useDisclosure} from '@nextui-org/modal';
import Button from '@/components/Button';

const FiltersModal = ({content}: { content: ReactElement }) => {
    const {isOpen, onOpen, onClose} = useDisclosure();

    return (
        <>
            <Button className='lg:hidden w-fit' onClick={onOpen} children='Фильтры'/>
            <Modal
                classNames={{
                    closeButton: 'p-3 !bg-transparent',
                    header: 'h-12 flex items-center px-2 py-0 text-white bg-zinc-800',
                    body: 'p-0 lg:items-center',
                }}
                size='full'
                isOpen={isOpen}
                onClose={onClose}
            >
                <ModalContent>
                    <ModalHeader children='Фильтры'/>
                    <ModalBody children={cloneElement(content, {onClose: onClose})}/>
                </ModalContent>
            </Modal>
        </>
    );
}

export default FiltersModal;
