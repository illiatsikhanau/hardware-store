import {BannerExt} from '@/services/banners';
import Link from 'next/link';
import Image from 'next/image';

const linkStyle = 'p-2 rounded-md border-1 border-zinc-200 duration-100 hover:shadow-card cursor-pointer';

const Banner = ({banner}: { banner: BannerExt }) => {
    const BannerImage = () => {
        return (
            <Image
                className='w-full h-full object-cover rounded-md'
                src={banner.src}
                alt='Изображение'
                width={600}
                height={250}
                sizes='
                    (max-width: 360px) 322px,
                    (max-width: 480px) 410px,
                    (max-width: 640px) 570px,
                    600px'
            />
        );
    }

    return (
        <>
            {banner.href ?
                <Link className={linkStyle} href={banner.href} aria-label={banner.href}>
                    <BannerImage/>
                </Link>
                :
                <div className={linkStyle}>
                    <BannerImage/>
                </div>
            }
        </>
    );
}

export default Banner;
