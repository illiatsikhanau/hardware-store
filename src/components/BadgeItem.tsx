import Link from 'next/link';
import HeartIcon from '@/components/svg/HeartIcon';
import CartIcon from '@/components/svg/CartIcon';
import BadgeCounter from '@/components/BadgeCounter';

const iconStyle = 'w-7 h-7 stroke-zinc-700 fill-none duration-100 group-hover:stroke-zinc-500';

const BadgeItem = async ({type}: { type: 'favorite' | 'cart' }) => {
    return (
        <Link className='flex group' href={`/${type}`}>
            {type === 'favorite' ?
                <HeartIcon className={iconStyle}/> :
                <CartIcon className={iconStyle}/>
            }
            <BadgeCounter type={type}/>
        </Link>
    )
}

export default BadgeItem;
