import {CategoryExt} from '@/services/categories';
import Link from 'next/link';
import Image from 'next/image';

const CategoryCard = ({category}: { category: CategoryExt }) => {
    return (
        <Link className='flex flex-col items-center duration-100 hover:opacity-80' href={`/catalog/${category.id}`}>
            <Image src={category.src} alt='Изображение' width={72} height={72}/>
            <p className='text-sm' children={category.name}/>
        </Link>
    );
}

export default CategoryCard;
