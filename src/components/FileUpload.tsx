'use client'

import {ComponentProps, useCallback, useState} from 'react';
import Spinner from '@/components/Spinner';
import {useDropzone} from 'react-dropzone';
import {uploadFile} from '@/services/upload';

const wrapperStyle = [
    'h-10 flex flex-1 justify-center border-1 border-zinc-200 bg-zinc-100 outline-none',
    'focus-visible:outline-2 focus-visible:outline-focus focus-visible:outline-offset-2',
].join(' ');

const spanStyle = [
    'h-10 w-full flex items-center justify-center rounded-md',
    'cursor-pointer transition-opacity duration-100 hover:opacity-90'
].join(' ');

const uploadStyle = [
    spanStyle,
    'border-1 border-zinc-700 bg-white text-zinc-700'
].join(' ');

interface FileUploadProps extends ComponentProps<'input'> {
    defaultContent: string | null
    onChangeFileUrl: (filename: string) => void
}

const FileUpload = (props: FileUploadProps) => {
    const [localFileName, setLocalFileName] = useState<string>();
    const [loading, setLoading] = useState(false);

    const onDrop = useCallback(async (acceptedFiles: Array<File>) => {
        const file = acceptedFiles[0];
        if (file) {
            setLoading(true);

            const fileUrl = await uploadFile(file);
            if (fileUrl) {
                setLocalFileName(file.name);
                props.onChangeFileUrl(fileUrl);
            }

            setLoading(false);
        }
    }, []);
    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop, accept: {'image/*': []}});

    return (
        <div{...getRootProps()} className={wrapperStyle}>
            <input {...getInputProps()}/>
            {loading ?
                <Spinner size='md'/> :
                isDragActive ?
                    <span
                        className={uploadStyle}
                        children='Загрузить файл'
                    /> :
                    <span
                        className={`${spanStyle} ${
                            localFileName || props.defaultContent ? 'bg-green' : 'bg-zinc-700'
                        } text-white`}
                        children={localFileName ?? props.defaultContent ?? props.content}
                    />
            }
        </div>
    );
}

export default FileUpload;
