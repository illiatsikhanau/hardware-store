import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import {Avatar, AvatarProps} from '@nextui-org/avatar';
import Image from 'next/image';

const CustomAvatar = async (props: AvatarProps) => {
    const session = await getServerSession(authOptions);
    const imageSrc = session?.user?.image;

    return <Avatar {...{
        ...props,
        icon: imageSrc ?
            <Image src={imageSrc} alt='Изображение' width={40} height={40}/> :
            session ?
                undefined :
                null,
        isBordered: true,
        classNames: {
            ...props.classNames,
            base: ['ring-zinc-300', session ? 'bg-accent' : 'bg-zinc-200', props.classNames?.base],
            icon: ['text-white', props.classNames?.base],
        }
    }}/>;
}

export default CustomAvatar;
