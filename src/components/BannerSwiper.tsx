'use client'

import {BannerExt} from '@/services/banners';
import 'swiper/css';
import 'swiper/css/autoplay';
import 'swiper/css/keyboard';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import {Swiper, SwiperSlide} from 'swiper/react';
import {Autoplay, Keyboard, Pagination, Navigation} from 'swiper/modules';
import Link from 'next/link';
import Image from 'next/image';

const linkStyle = 'duration-100 hover:opacity-80 cursor-pointer';

const BannerSwiper = ({banners}: { banners: BannerExt[] }) => {
    const BannerImage = ({banner, bIndex}: { banner: BannerExt, bIndex: number }) => {
        return (
            <Image
                className='w-full h-full object-cover'
                src={banner.src}
                alt='Изображение'
                width={464}
                height={186}
                sizes='
                    (max-width: 360px) 336px,
                    (max-width: 480px) 464px,
                    (max-width: 640px) 624px,
                    (max-width: 768px) 752px,
                    (max-width: 1024px) 1008px,
                    1264px'
                priority={!bIndex}
                placeholder={!!bIndex ? 'blur' : undefined}
                blurDataURL={!!bIndex ? '/placeholder.svg' : undefined}
            />
        );
    }

    return (
        <div className='w-full p-1'>
            {banners.length > 0 &&
                <Swiper
                    loop={true}
                    autoplay={{
                        delay: 4000,
                        disableOnInteraction: false,
                    }}
                    keyboard={true}
                    pagination={{clickable: true}}
                    navigation={true}
                    modules={[Autoplay, Keyboard, Pagination, Navigation]}
                >
                    {banners.map((banner, bIndex) =>
                        <SwiperSlide key={banner.id}>
                            {banner.href ?
                                <Link className={linkStyle} href={banner.href} aria-label={banner.href}>
                                    <BannerImage banner={banner} bIndex={bIndex}/>
                                </Link>
                                :
                                <div className={linkStyle}>
                                    <BannerImage banner={banner} bIndex={bIndex}/>
                                </div>
                            }
                        </SwiperSlide>
                    )}
                </Swiper>
            }
        </div>
    );
}

export default BannerSwiper;
