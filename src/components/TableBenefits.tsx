'use client'

import {useEffect, useState, KeyboardEvent} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveBenefit, updateActiveBenefit} from '@/redux/adminReducer';
import {getBenefits, createBenefit, updateBenefit, deleteBenefit, BenefitExt, EMPTY_BENEFIT} from '@/services/benefits';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import {Tooltip} from '@nextui-org/tooltip';
import Image from 'next/image';
import Button from '@/components/Button';
import Spinner from '@/components/Spinner';
import Input from '@/components/Input';
import FileUpload from '@/components/FileUpload';

const helpStyle = 'w-5 h-5 flex items-center justify-center rounded-full bg-zinc-700 text-white cursor-pointer';

const TableBenefits = () => {
    const dispatch = useAppDispatch();
    const activeBenefit = useAppSelector(state => state.admin.activeBenefit);

    const [benefits, setBenefits] = useState<BenefitExt[]>();

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitBenefit = async () => {
        if (activeBenefit.name && activeBenefit.icon) {
            const newBenefit = activeBenefit.id ?
                await updateBenefit(activeBenefit) :
                await createBenefit(activeBenefit);

            if (benefits && newBenefit) {
                await updateBenefits().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitBenefit();
        }
    }

    const removeBenefit = async () => {
        const success = await deleteBenefit(activeBenefit.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateBenefits().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectBenefit = (benefit: BenefitExt) => {
        dispatch(setActiveBenefit(benefit));
        onOpen();
    }

    const updateBenefits = async () => {
        const benefits = await getBenefits();
        setBenefits(benefits);
    }

    useEffect(() => {
        void updateBenefits();
    }, []);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveBenefit(EMPTY_BENEFIT));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='flex flex-col gap-2'>
            {benefits ?
                <>
                    <Button
                        className='w-fit bg-green'
                        onClick={onOpen}
                        children='Добавить'
                    />
                    <Table aria-label='Преимущества' removeWrapper hideHeader={!benefits.length}>
                        <TableHeader>
                            <TableColumn className='!rounded-l-md bg-zinc-800 text-white' children='Название'/>
                            <TableColumn className='!rounded-r-md bg-zinc-800 text-white' children='Иконка'/>
                        </TableHeader>
                        <TableBody emptyContent='Нет преимуществ'>
                            {benefits.map(benefit =>
                                <TableRow
                                    key={benefit.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectBenefit(benefit)}
                                >
                                    <TableCell className='rounded-l-md' children={benefit.name}/>
                                    <TableCell className='rounded-r-md'>
                                        <Image src={benefit.icon} alt='Изображение' width={48} height={48}/>
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
                size='xl'
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeBenefit.id ? 'Редактирование' : 'Добавление'} преимущества
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название преимущества'
                            value={activeBenefit.name}
                            onChange={e => dispatch(updateActiveBenefit(['name', e.target.value]))}
                            autoFocus
                        />
                        <div className='flex gap-2'>
                            <FileUpload
                                content='Выберите иконку'
                                defaultContent={activeBenefit.icon ? 'Текущая иконка' : null}
                                onChangeFileUrl={url => dispatch(updateActiveBenefit(['icon', url]))}
                            />
                            <div className='hidden md:flex items-center'>
                                <Tooltip placement='top' content='Рекомендуемый формат - svg'>
                                    <span className={helpStyle} children='?'/>
                                </Tooltip>
                            </div>
                        </div>
                        <Input
                            classNames={{input: '!p-0'}}
                            type='url'
                            startContent={
                                <span className='text-sm text-zinc-400 whitespace-nowrap' children={process.env.URL}/>
                            }
                            value={activeBenefit.href ?? ''}
                            onChange={e => dispatch(updateActiveBenefit(['href', e.target.value.trim()]))}
                        />
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeBenefit.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить преимущество "${activeBenefit.name}"`}
                                    onConfirm={removeBenefit}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitBenefit}
                            isDisabled={!activeBenefit.name || !activeBenefit.icon}
                            children={activeBenefit.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableBenefits;
