'use client'

import {usePathname} from 'next/navigation';
import {CategoryExt} from '@/services/categories';
import Link from 'next/link';
import Image from 'next/image';

const imageStyle = [
    'w-6 h-6 duration-100 group-hover:brightness-0',
    'group-hover:invert group-[.active]:brightness-0 group-[.active]:invert',
].join(' ');

const CategoryLabel = ({category, index}: { category: CategoryExt, index?: boolean }) => {
    const pathname = usePathname();
    const getActiveStyle =
        (pathname === `/catalog/${category.id}`) || (pathname === '/catalog' && index) ?
            'active bg-zinc-800' :
            ''

    return (
        <Link className={`${getActiveStyle} w-full flex gap-1 p-2 rounded-md duration-100 hover:bg-zinc-800 group`}
              href={`/catalog/${category.id}`}>
            <Image
                className={imageStyle}
                src={category.icon}
                alt='Изображение'
                width={24}
                height={24}
            />
            <p
                className='text-base whitespace-nowrap duration-100 group-hover:text-white group-[.active]:text-white'
                children={category.name}
            />
        </Link>
    );
}

export default CategoryLabel;
