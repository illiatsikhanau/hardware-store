'use client'

import {usePathname, useRouter} from 'next/navigation';
import {Select, SelectItem} from '@nextui-org/select';

interface MenuItem {
    name: string
    href: string
}

const MenuSelect = ({index, items}: { index: string, items: MenuItem[] }) => {
    const pathname = usePathname();
    const router = useRouter();

    return (
        <Select
            classNames={{
                base: 'flex md:hidden',
                trigger: 'h-10 rounded-md !bg-zinc-800',
                innerWrapper: 'gap-1',
                selectorIcon: 'stroke-white',
                value: 'font-bold !text-white text-lg',
            }}
            listboxProps={{itemClasses: {base: 'rounded-md bg-zinc-800 text-white'}}}
            popoverProps={{classNames: {content: 'rounded-md bg-zinc-800'}}}
            aria-label='Меню'
            selectedKeys={[
                pathname === index ?
                    items.length ?
                        items[0].href :
                        '' :
                    pathname
            ]}
            onChange={e => router.push(e.target.value)}
            scrollShadowProps={{isEnabled: false}}
        >
            {items.map(item =>
                <SelectItem key={item.href} children={item.name}/>
            )}
        </Select>
    );
}

export default MenuSelect;
