'use client'

import {ChangeEvent, useEffect, useState} from 'react';
import {Role} from '@prisma/client';
import {getUsers, getUsersInfo, updateUser, UserExt, USERS_PER_PAGE} from '@/services/users';
import {Table, TableHeader, TableBody, TableColumn, TableRow, TableCell} from '@nextui-org/table';
import {User as UserAvatar} from '@nextui-org/user';
import {Select, SelectItem} from '@nextui-org/select';
import Spinner from '@/components/Spinner';
import Pagination from '@/components/Pagination';
import TableSearch from '@/components/TableSearch';

const avatarStyle = 'max-sm:w-20 sm:w-44 md:w-72';

const TableUsers = ({email}: { email: string }) => {
    const [users, setUsers] = useState<UserExt[]>();
    const [page, setPage] = useState(1);
    const [pages, setPages] = useState(1);
    const [search, setSearch] = useState('');

    const updateRole = async (e: ChangeEvent<HTMLSelectElement>, user: UserExt) => {
        const value = e.target.value as Role;
        if (value) {
            const newUser = await updateUser({...user, role: value});
            if (users && newUser) {
                void updateUsers();
            }
        }
    }

    const updateUsers = async () => {
        const query = {
            email: `contains:${search}`,
            skip: (page - 1) * USERS_PER_PAGE,
        }
        const info = await getUsersInfo();
        const users = await getUsers(query);
        setPages(Math.ceil(info.count / USERS_PER_PAGE));
        setUsers(users);
    }

    useEffect(() => {
        void updateUsers();
    }, [page]);

    return (
        <div className='w-full flex flex-col gap-2'>
            {users ?
                <>
                    <div className='flex max-lg:flex-col items-center justify-between gap-y-2'>
                        <TableSearch
                            search={search}
                            setSearch={setSearch}
                            onSearch={updateUsers}
                        />
                        {(pages > 1 || page > 1) &&
                            <Pagination total={pages} page={page} onChange={page => setPage(page)}/>
                        }
                    </div>
                    <Table aria-label='Пользователи' removeWrapper>
                        <TableHeader>
                            <TableColumn className='!rounded-l-md bg-zinc-800 text-white' children='Пользователь'/>
                            <TableColumn className='!rounded-r-md bg-zinc-800 text-white' children='Роль'/>
                        </TableHeader>
                        <TableBody emptyContent='Нет пользователей'>
                            {users.map(user =>
                                <TableRow key={user.id} className='duration-100 hover:bg-zinc-200'>
                                    <TableCell className='rounded-l-md'>
                                        <UserAvatar
                                            classNames={{
                                                name: [avatarStyle, 'truncate'],
                                                description: [avatarStyle, 'truncate'],
                                                wrapper: avatarStyle,
                                            }}
                                            avatarProps={{
                                                src: user.image ?? undefined,
                                                isBordered: true,
                                                classNames: {base: 'ring-zinc-300'}
                                            }}
                                            description={user.email}
                                            name={user.email}
                                            children={user.email}
                                        />
                                    </TableCell>
                                    <TableCell className='rounded-r-md'>
                                        <Select
                                            classNames={{
                                                trigger: 'h-10 rounded-md !bg-zinc-800 text-white',
                                                selectorIcon: 'stroke-white',
                                                value: '!text-white',
                                            }}
                                            listboxProps={{itemClasses: {base: 'rounded-md bg-zinc-800 text-white'}}}
                                            popoverProps={{classNames: {content: 'p-1 rounded-md bg-zinc-800 text-white'}}}
                                            selectedKeys={[user.role]}
                                            onChange={e => updateRole(e, user)}
                                            aria-label='Роль'
                                            isDisabled={user.email === email}
                                        >
                                            {Object.keys(Role).map(role =>
                                                <SelectItem key={role} children={role}/>
                                            )}
                                        </Select>
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
        </div>
    );
}

export default TableUsers;
