import moment from 'moment';
import {BlogExt} from '@/services/blogs';
import Link from 'next/link';
import Chip from '@/components/Chip';
import Image from 'next/image';

const linkStyle = [
    'flex w-full gap-2 p-2 rounded-md border-1 border-zinc-200 bg-zinc-100 duration-100',
    'hover:bg-zinc-800 overflow-hidden group'
].join(' ');

const BlogCard = ({blog, priority}: { blog: BlogExt, priority?: boolean }) => {
    return (
        <Link className={linkStyle} href={blog.href}>
            <Image
                className='w-42 h-28 sm:w-60 sm:h-40 rounded-md'
                src={blog.src}
                alt='Изображение'
                width={168}
                height={112}
                sizes='
                    (max-width: 640px) 168px,
                    240px'
                priority={priority}
            />
            <div className='flex w-full flex-col gap-1'>
                {blog.type &&
                    <div className='flex h-fit'>
                        <Chip
                            classNames={{
                                base: 'bg-accent group-hover:bg-white',
                                content: 'group-hover:text-zinc-700',
                            }}
                            size='sm'
                            children={blog.type.name.toUpperCase()}
                        />
                    </div>
                }
                <p
                    className='flex h-full sm:text-lg max-xs:break-all duration-100 group-hover:text-white'
                    children={blog.name}
                />
                <b
                    className='flex justify-end uppercase text-xs text-zinc-600 duration-100 group-hover:text-white'
                    children={blog.createdAt ? moment(blog.createdAt).format('DD.MM.YYYY') : ''}
                />
            </div>
        </Link>
    );
}

export default BlogCard;
