'use client'

import {usePathname} from 'next/navigation';
import Link from 'next/link';

const CategoryLabel = ({name, href}: { name: string, href: string }) => {
    const pathname = usePathname();
    const getActiveStyle = pathname === href ? 'active bg-zinc-800' : '';

    return (
        <Link
            className={`${getActiveStyle} w-full flex gap-1 p-2 rounded-md duration-100 hover:bg-zinc-800 group`}
            href={href}
        >
            <b
                className='whitespace-nowrap duration-100 group-hover:text-white group-[.active]:text-white'
                children={name}
            />
        </Link>
    );
}

export default CategoryLabel;
