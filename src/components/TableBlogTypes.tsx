'use client'

import {KeyboardEvent, useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveBlogType, updateActiveBlogType} from '@/redux/adminReducer';
import {
    getBlogTypes, createBlogType, updateBlogType, deleteBlogType,
    BlogTypeExt, EMPTY_BLOG_TYPE,
} from '@/services/blogTypes';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import Button from '@/components/Button';
import Input from '@/components/Input';

interface TableBlogTypesProps {
    blogTypes: BlogTypeExt[],
    setBlogTypes: (blogTypes: BlogTypeExt[]) => void
}

const TableBlogTypes = (props: TableBlogTypesProps) => {
    const dispatch = useAppDispatch();
    const activeBlogType = useAppSelector(state => state.admin.activeBlogType);

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitBlogType = async () => {
        if (activeBlogType.name) {
            const newBlogType = activeBlogType.id ?
                await updateBlogType(activeBlogType) :
                await createBlogType(activeBlogType);

            if (newBlogType) {
                await updateBlogTypes().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitBlogType();
        }
    }

    const removeBlogType = async () => {
        const success = await deleteBlogType(activeBlogType.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateBlogTypes().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectBlogType = (blogType: BlogTypeExt) => {
        dispatch(setActiveBlogType(blogType));
        onOpen();
    }

    const updateBlogTypes = async () => {
        const blogTypes = await getBlogTypes();
        props.setBlogTypes(blogTypes);
    }

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveBlogType(EMPTY_BLOG_TYPE));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='w-full flex flex-col gap-2'>
            <Button
                className='w-fit bg-green'
                onClick={onOpen}
                children='Добавить'
            />
            <Table aria-label='Типы статей' removeWrapper hideHeader={!props.blogTypes.length}>
                <TableHeader>
                    <TableColumn className='!rounded-md bg-zinc-800 text-white' children='Название'/>
                </TableHeader>
                <TableBody emptyContent='Нет типов статей'>
                    {props.blogTypes.map(blogType =>
                        <TableRow
                            key={blogType.id}
                            className='cursor-pointer duration-100 hover:bg-zinc-200'
                            onClick={() => selectBlogType(blogType)}
                        >
                            <TableCell className='rounded-md' children={blogType.name}/>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeBlogType.id ? 'Редактирование' : 'Добавление'} типа статьи
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название типа статьи'
                            value={activeBlogType.name}
                            onChange={e => dispatch(updateActiveBlogType(['name', e.target.value]))}
                            autoFocus
                        />
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeBlogType.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить тип статьи "${activeBlogType.name}"?`}
                                    onConfirm={removeBlogType}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitBlogType}
                            isDisabled={!activeBlogType.name}
                            children={activeBlogType.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableBlogTypes;
