import {ComponentProps} from 'react';

const SearchIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg className={props.className ?? ''} viewBox='-0.5 0 25 25' fill='none'>
            <path
                d='M15 14.32C18.3137 14.32 21 11.6337 21 8.32001C21 5.0063 18.3137 2.32001 15 2.32001C11.6863 2.32001 9 5.0063 9 8.32001C9 11.6337 11.6863 14.32 15 14.32Z'
                strokeWidth='1.7' strokeLinecap='round' strokeLinejoin='round'/>
            <path d='M8 19.32L6 17.32' strokeWidth='1.7' strokeLinecap='round' strokeLinejoin='round'/>
            <path d='M5 22.32L3 20.32' strokeWidth='1.7' strokeLinecap='round' strokeLinejoin='round'/>
            <path d='M3 20.32L10.76 12.56' strokeWidth='1.7' strokeLinecap='round' strokeLinejoin='round'/>
        </svg>
    );
}

export default SearchIcon;
