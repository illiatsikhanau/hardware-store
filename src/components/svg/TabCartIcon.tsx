import {ComponentProps} from 'react';

const TabCartIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg className={props.className ?? ''} viewBox='-5 -6 36.00 36.00' fill='none' strokeLinecap='round'
             stroke='#3f3f46' strokeLinejoin='round' strokeWidth='1.2'>
            <path
                d='M5.33331 6H19.8672C20.4687 6 20.9341 6.52718 20.8595 7.12403L20.1095 13.124C20.0469 13.6245 19.6215 14 19.1172 14H16.5555H9.44442H7.99998'/>
            <path
                d='M2 4H4.23362C4.68578 4 5.08169 4.30341 5.19924 4.74003L8.30076 16.26C8.41831 16.6966 8.81422 17 9.26638 17H19'/>
            <circle cx='10' cy='20' r='1'/>
            <circle cx='17.5' cy='20' r='1'/>
        </svg>
    );
}

export default TabCartIcon;
