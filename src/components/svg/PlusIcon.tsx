import {ComponentProps} from 'react';

const PlusIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg className={props.className ?? ''} viewBox='0 0 24 24'>
            <path d='M4 12H20M12 4V20' strokeWidth='3' strokeLinecap='round' strokeLinejoin='round'/>
        </svg>
    );
}

export default PlusIcon;
