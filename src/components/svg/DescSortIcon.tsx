import {ComponentProps} from 'react';

const DescSortIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg
            className={props.className ?? ''}
            viewBox='0 0 24 24'
            fill='#3f3f46'
            strokeLinecap='round'
            strokeLinejoin='round'
        >
            <path
                d='M6,20a1,1,0,0,1-.71-.29l-4-4a1,1,0,0,1,1.42-1.42L6,17.59l3.29-3.3a1,1,0,0,1,1.42,1.42l-4,4A1,1,0,0,1,6,20Z'/>
            <path d='M6,20a1,1,0,0,1-1-1V4A1,1,0,0,1,7,4V19A1,1,0,0,1,6,20Z'/>
            <path d='M20,17H15a1,1,0,0,1,0-2h5a1,1,0,0,1,0,2Z'/>
            <path d='M20,12H13a1,1,0,0,1,0-2h7a1,1,0,0,1,0,2Z'/>
            <path d='M20,7H10a1,1,0,0,1,0-2H20a1,1,0,0,1,0,2Z'/>
        </svg>
    );
}

export default DescSortIcon;
