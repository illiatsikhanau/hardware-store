import {ComponentProps} from 'react';

const MenuIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg className={props.className ?? ''} viewBox='0 0 24 24' transform='matrix(-1, 0, 0, 1, 0, 0)'
             strokeWidth='2.25' strokeLinecap='round' strokeLinejoin='round'>
            <path d='M12 18H20M4 12H20M4 6H20'/>
        </svg>
    );
}

export default MenuIcon;
