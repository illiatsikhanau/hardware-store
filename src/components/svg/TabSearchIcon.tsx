import {ComponentProps} from 'react';

const TabSearchIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg className={props.className ?? ''} viewBox='-8.4 -8.4 40.80 40.80' fill='none' strokeWidth='1.32'
             strokeLinecap='round' stroke='#3f3f46' strokeLinejoin='round'>
            <path
                d='M14.9536 14.9458L21 21M17 10C17 13.866 13.866 17 10 17C6.13401 17 3 13.866 3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10Z'/>
        </svg>
    );
}

export default TabSearchIcon;
