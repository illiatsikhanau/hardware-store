import {ComponentProps} from 'react';

const MinusIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg className={props.className ?? ''} viewBox='0 0 24 24'>
            <path d='M6 12L18 12' strokeWidth='3' strokeLinecap='round' strokeLinejoin='round'/>
        </svg>
    );
}

export default MinusIcon;
