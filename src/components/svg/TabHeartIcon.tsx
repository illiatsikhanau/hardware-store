import {ComponentProps} from 'react';

const TabHeartIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg className={props.className ?? ''} viewBox='-9.6 -9.6 43.20 43.20' fill='none' strokeLinecap='round'
             stroke='#3f3f46' strokeLinejoin='round' strokeWidth='1.44'>
            <path
                d='M21 8.99998C21 12.7539 15.7156 17.9757 12.5857 20.5327C12.2416 20.8137 11.7516 20.8225 11.399 20.5523C8.26723 18.1523 3 13.1225 3 8.99998C3 2.00001 12 2.00002 12 8C12 2.00001 21 1.99999 21 8.99998Z'/>
        </svg>
    );
}

export default TabHeartIcon;
