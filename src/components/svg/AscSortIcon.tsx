import {ComponentProps} from 'react';

const AscSortIcon = (props: ComponentProps<'svg'>) => {
    return (
        <svg
            className={props.className ?? ''}
            viewBox='0 0 24 24'
            fill='#3f3f46'
            strokeLinecap='round'
            strokeLinejoin='round'
        >
            <path
                d='M10,9a1,1,0,0,1-.71-.29L6,5.41,2.71,8.71A1,1,0,0,1,1.29,7.29l4-4a1,1,0,0,1,1.42,0l4,4a1,1,0,0,1,0,1.42A1,1,0,0,1,10,9Z'/>
            <path d='M6,20a1,1,0,0,1-1-1V4A1,1,0,0,1,7,4V19A1,1,0,0,1,6,20Z'/>
            <path d='M20,8H15a1,1,0,0,1,0-2h5a1,1,0,0,1,0,2Z'/>
            <path d='M20,13H13a1,1,0,0,1,0-2h7a1,1,0,0,1,0,2Z'/>
            <path d='M20,18H10a1,1,0,0,1,0-2H20a1,1,0,0,1,0,2Z'/>
        </svg>
    );
}

export default AscSortIcon;
