import {useEffect} from 'react';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import Button from '@/components/Button';

interface ModalDeleteConfirmationProps {
    isVisible: boolean
    toggleVisible: () => void
    content: string
    onConfirm: () => Promise<void>
}

const ModalDeleteConfirmation = (props: ModalDeleteConfirmationProps) => {
    const {isOpen, onOpen, onClose} = useDisclosure();

    const onConfirm = async () => {
        await props.onConfirm();
        props.toggleVisible();
    }

    useEffect(() => {
        if (props.isVisible) {
            onOpen();
        } else {
            onClose();
        }
    }, [props.isVisible]);

    return (
        <Modal hideCloseButton placement='center' isOpen={isOpen} onClose={props.toggleVisible}>
            <ModalContent>
                <ModalHeader children='Подтвердите удаление'/>
                <ModalBody children={props.content}/>
                <ModalFooter>
                    <Button
                        className='bg-transparent text-main border-2 border-main'
                        onClick={props.toggleVisible}
                        children='Отмена'
                    />
                    <Button
                        className='bg-main'
                        onClick={onConfirm}
                        children='Удалить'
                    />
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
}

export default ModalDeleteConfirmation;
