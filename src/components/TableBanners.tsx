'use client'

import {useEffect, useState, KeyboardEvent} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveBanner, updateActiveBanner} from '@/redux/adminReducer';
import {
    getBanners, getBannersInfo, createBanner, updateBanner, deleteBanner,
    BannerExt, EMPTY_BANNER, BANNERS_PER_PAGE
} from '@/services/banners';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {getDateString, isEqualDates} from '@/helpers/date';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import {Tooltip} from '@nextui-org/tooltip';
import Image from 'next/image';
import Button from '@/components/Button';
import Spinner from '@/components/Spinner';
import Input from '@/components/Input';
import FileUpload from '@/components/FileUpload';
import Pagination from '@/components/Pagination';
import DatePicker from '@/components/DatePicker';

const helpStyle = 'w-5 h-5 flex items-center justify-center rounded-full bg-zinc-700 text-white cursor-pointer';

const TableBanners = () => {
    const dispatch = useAppDispatch();
    const activeBanner = useAppSelector(state => state.admin.activeBanner);

    const [banners, setBanners] = useState<BannerExt[]>();
    const [page, setPage] = useState(1);
    const [pages, setPages] = useState(1);

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitBanner = async () => {
        if (activeBanner.src) {
            const newBanner = activeBanner.id ?
                await updateBanner(activeBanner) :
                await createBanner(activeBanner);

            if (banners && newBanner) {
                await updateBanners().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitBanner();
        }
    }

    const removeBanner = async () => {
        const success = await deleteBanner(activeBanner.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateBanners().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectBanner = (banner: BannerExt) => {
        dispatch(setActiveBanner(banner));
        onOpen();
    }

    const updateBanners = async () => {
        const info = await getBannersInfo();
        const banners = await getBanners({skip: (page - 1) * BANNERS_PER_PAGE});
        setPages(Math.ceil(info.count / BANNERS_PER_PAGE));
        setBanners(banners);
    }

    useEffect(() => {
        void updateBanners();
    }, [page]);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveBanner(EMPTY_BANNER));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='flex flex-col gap-2'>
            {banners ?
                <>
                    <div className='flex max-lg:flex-col items-center justify-between gap-y-2'>
                        <Button
                            className='min-w-unit-24 bg-green'
                            onClick={onOpen}
                            children='Добавить'
                        />
                        {(pages > 1 || page > 1) &&
                            <Pagination total={pages} page={page} onChange={page => setPage(page)}/>
                        }
                    </div>
                    <Table aria-label='Баннеры' removeWrapper hideHeader={!banners.length}>
                        <TableHeader>
                            <TableColumn
                                className='!rounded-l-md bg-zinc-800 text-white'
                                children='Картинка'
                            />
                            <TableColumn
                                className='!rounded-r-md bg-zinc-800 text-white'
                                children='Дата окончания'
                            />
                        </TableHeader>
                        <TableBody emptyContent='Нет баннеров'>
                            {banners.map(banner =>
                                <TableRow
                                    key={banner.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectBanner(banner)}
                                >
                                    <TableCell className='rounded-l-md'>
                                        <Image
                                            src={banner.src}
                                            alt='Изображение'
                                            width={384}
                                            height={160}
                                        />
                                    </TableCell>
                                    <TableCell
                                        className='rounded-r-md'
                                        children={getDateString(banner.expiresAt)}
                                    />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
                size='xl'
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeBanner.id ? 'Редактирование' : 'Добавление'} баннера
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <div className='flex gap-2'>
                            <FileUpload
                                content='Выберите картинку'
                                defaultContent={activeBanner.src ? 'Текущая картинка' : null}
                                onChangeFileUrl={url => dispatch(updateActiveBanner(['src', url]))}
                            />
                            <div className='hidden md:flex items-center'>
                                <Tooltip placement='top' content='Рекомендуемый формат - jpg/webp 1200x500 пикселей'>
                                    <span className={helpStyle} children='?'/>
                                </Tooltip>
                            </div>
                        </div>
                        <Input
                            classNames={{input: '!p-0'}}
                            type='url'
                            startContent={
                                <span className='text-sm text-zinc-400 whitespace-nowrap' children={process.env.URL}/>
                            }
                            value={activeBanner.href ?? ''}
                            onChange={e => dispatch(updateActiveBanner(['href', e.target.value.trim()]))}
                            autoFocus
                        />
                        <div className='flex flex-col items-center gap-1'>
                            <p className='text-zinc-700'>
                                Дата окончания: <b>{getDateString(activeBanner.expiresAt) || 'нет'}</b>
                            </p>
                            <DatePicker
                                selected={activeBanner.expiresAt ? new Date(activeBanner.expiresAt?.toString()) : null}
                                onSelect={date => dispatch(updateActiveBanner(
                                    ['expiresAt', isEqualDates(activeBanner.expiresAt, date) ? date : null]
                                ))}
                                onChange={() => {
                                }}
                                minDate={new Date()}
                            />
                        </div>
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeBanner.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content='Вы действительно хотите удалить баннер'
                                    onConfirm={removeBanner}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitBanner}
                            isDisabled={!activeBanner.src}
                            children={activeBanner.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableBanners;
