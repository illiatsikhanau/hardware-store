import Input from '@/components/Input';
import SearchIcon from '@/components/svg/SearchIcon';

const Search = () => {
    return (
        <div className='flex w-full items-center'>
            <Input
                classNames={{
                    base: '-mr-10',
                    inputWrapper: 'pr-10'
                }}
                placeholder='Поиск'
            />
            <SearchIcon className='w-10 h-10 p-2 stroke-zinc-700 cursor-pointer z-20'/>
        </div>
    );
}

export default Search;
