'use client'

import {useEffect, useState} from 'react';
import {usePathname, useRouter, useSearchParams} from 'next/navigation';
import Checkbox from '@/components/Checkbox';
import PriceSlider from '@/components/PriceSlider';
import Button from '@/components/Button';
import {updateSearchParams} from '@/helpers/searchParams';

const itemWrapperStyle = 'w-full sm:max-lg:w-1/2 flex items-center p-2';

interface FiltersListProps {
    minPrice: number
    maxPrice: number
    onClose?: () => void
}

const FiltersList = (props: FiltersListProps) => {
    const pathname = usePathname();
    const searchParams = useSearchParams();
    const router = useRouter();

    const [count, setCount] = useState(true);
    const updateCount = (value: boolean) => setCount(value);
    const [priceSliderValue, setPriceSliderValue] = useState([props.minPrice, props.maxPrice]);

    useEffect(() => {
        setCount(!searchParams.get('count'));

        const priceProps = searchParams.get('price')
            ?.match(/\d+(\.\d+)?/g)
            ?.map(match => parseFloat(match));
        setPriceSliderValue(
            priceProps?.length === 2 ?
                priceProps :
                [props.minPrice, props.maxPrice]
        );
    }, [searchParams])

    const applyFilters = () => {
        updateSearchParams(router, pathname, searchParams, [
            {name: 'count', value: count ? '' : 'gte:0'},
            {name: 'price', value: `gte:${priceSliderValue[0]},lte:${priceSliderValue[1]}`},
        ]);
        cancel();
    }

    const resetFilters = () => {
        updateSearchParams(router, pathname, searchParams, [
            {name: 'count', value: ''},
            {name: 'price', value: ''},
        ]);
        cancel();
    }

    const cancel = () => {
        props.onClose && props.onClose();
    }

    return (
        <div className='lg:w-60 flex flex-wrap'>
            <div className={itemWrapperStyle}>
                <Checkbox
                    classNames={{label: 'text-zinc-700'}}
                    isSelected={count}
                    onValueChange={updateCount}
                    children='Товары в наличии'
                />
            </div>
            {props.minPrice !== props.maxPrice &&
                <div className={itemWrapperStyle}>
                    <PriceSlider
                        minPrice={props.minPrice}
                        maxPrice={props.maxPrice}
                        price={priceSliderValue}
                        setPrice={setPriceSliderValue}
                    />
                </div>
            }
            <div className={itemWrapperStyle}>
                <Button
                    className='w-full'
                    onClick={applyFilters}
                    children='Применить фильтры'
                />
            </div>
            <div className={itemWrapperStyle}>
                <Button
                    className='w-full bg-zinc-500'
                    onClick={resetFilters}
                    children='Сбросить фильтры'
                />
            </div>
            <div className={`lg:hidden ${itemWrapperStyle}`}>
                <Button
                    className='w-full bg-transparent text-main border-2 border-main'
                    onClick={cancel}
                    children='Отмена'
                />
            </div>
        </div>
    );
}

export default FiltersList;
