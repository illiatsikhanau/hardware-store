'use client'

import ToTopIcon from '@/components/svg/ToTopIcon';

const style = [
    'hidden fixed right-4 bottom-24 lg:bottom-4 w-14 h-14 opacity-70 z-40 cursor-pointer',
    'duration-100 hover:bottom-25 hover:lg:bottom-5'
].join(' ');

const ToTopButton = () => {
    const scrollTop = () => {
        window.scrollTo({top: 0});
    }

    return (
        <div id='to-top-button' className={style} onClick={scrollTop}>
            <ToTopIcon/>
        </div>
    );
}

export default ToTopButton;
