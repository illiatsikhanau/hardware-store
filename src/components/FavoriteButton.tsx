'use client'

import {useState} from 'react';
import {useAppDispatch} from '@/redux/hooks';
import {incFavoriteCount, decFavoriteCount} from '@/redux/userReducer';
import {addFavorite, deleteFavorite} from '@/services/favorite';
import {ProductExt} from '@/services/products';
import HeartIcon from '@/components/svg/HeartIcon';

interface FavoriteButtonProps {
    product: ProductExt
    size: 'md' | 'sm'
}

const FavoriteButton = (props: FavoriteButtonProps) => {
    const dispatch = useAppDispatch();

    const [isFavorite, setIsFavorite] = useState(props.product.isFavorite);
    const [disabled, setDisabled] = useState(false);

    const toggleIsFavorite = async () => {
        if (!disabled) {
            setDisabled(true);
            dispatch(isFavorite ? decFavoriteCount() : incFavoriteCount());
            setIsFavorite(!isFavorite);

            if (isFavorite) {
                await deleteFavorite(props.product.id);
            } else {
                await addFavorite(props.product.id);
            }

            setDisabled(false);
        }
    }

    const favoriteStyle = [
        props.size === 'sm' ? 'w-6 h-6' : 'w-10 h-10 p-1',
        'flex rounded-bl-md duration-100 group-hover:opacity-100 hover:opacity-100',
        'bg-white z-20 cursor-pointer',
        isFavorite || props.size === 'sm' ? 'opacity-100' : 'opacity-0'
    ].join(' ');

    const favoriteIconStyle = [
        'flex fill-none duration-100 group-hover:stroke-zinc-500 hover:stroke-zinc-500',
        isFavorite ? '!stroke-main' : '',
        !isFavorite && props.size === 'sm' ? 'stroke-zinc-300' : '',
        !isFavorite && props.size === 'md' ? 'stroke-zinc-500 p-1' : '',
    ].join(' ');

    return (
        <div className={favoriteStyle} onClick={toggleIsFavorite}>
            <HeartIcon className={favoriteIconStyle}/>
        </div>
    );
}

export default FavoriteButton;
