import {Input, InputProps} from '@nextui-org/input';

const CustomInput = (props: InputProps) => {
    return <Input {...{
        ...props,
        labelPlacement: props.labelPlacement ?? 'outside-left',
        classNames: {
            ...props.classNames,
            mainWrapper: ['w-full', props.classNames?.mainWrapper],
            inputWrapper: [
                'rounded-md border-1 border-zinc-200 shadow-none bg-zinc-100 text-zinc-700',
                '!duration-100 placeholder:text-zinc-400',
                props.classNames?.inputWrapper
            ],
            input: ['text-base', props.classNames?.input],
            label: ['text-base text-zinc-500', props.classNames?.label],
        },
    }}/>
}

export default CustomInput;
