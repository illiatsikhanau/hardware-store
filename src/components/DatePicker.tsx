'use client'

import 'react-datepicker/dist/react-datepicker.css';
import DatePicker, {registerLocale} from 'react-datepicker';
import {ReactDatePickerProps} from 'react-datepicker';
import ru from 'date-fns/locale/ru';

registerLocale('ru', ru);

const CustomDatePicker = (props: ReactDatePickerProps) => {
    return (
        <DatePicker {...{
            ...props,
            locale: props.locale ?? 'ru',
            inline: props.inline ?? true
        }}/>
    );
}

export default CustomDatePicker;
