import {Spinner, SpinnerProps} from '@nextui-org/spinner';

const CustomSpinner = (props: SpinnerProps) => {
    return (
        <Spinner {...{
            ...props,
            classNames: {
                ...props.classNames,
                circle1: ['border-b-accent', props.classNames?.circle1],
                circle2: ['border-b-accent', props.classNames?.circle2],
            },
            size: props.size ?? 'lg'
        }}/>
    );
}

export default CustomSpinner;
