import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import {Role} from '@prisma/client';
import Link from 'next/link';
import Button from '@/components/Button';
import Input from '@/components/Input';
import Checkbox from '@/components/Checkbox';
import TikTokIcon from '@/components/svg/TikTokIcon';
import YouTubeIcon from '@/components/svg/YouTubeIcon';
import TelegramIcon from '@/components/svg/TelegramIcon';
import ViberIcon from '@/components/svg/ViberIcon';
import VkIcon from '@/components/svg/VkIcon';
import CashIcon from '@/components/svg/CashIcon';
import MastercardIcon from '@/components/svg/MastercardIcon';
import MaestroIcon from '@/components/svg/MaestroIcon';
import MirIcon from '@/components/svg/MirIcon';
import SbpIcon from '@/components/svg/SbpIcon';
import VisaIcon from '@/components/svg/VisaIcon';
import VisaElectronIcon from '@/components/svg/VisaElectronIcon';
import UnionPayIcon from '@/components/svg/UnionPayIcon';

const newsletterStyle = 'flex max-w-fit lg:w-full lg:max-w-7xl flex-col lg:flex-row justify-center gap-y-4 gap-x-2 p-4';
const menuStyle = 'flex w-full flex-col justify-center overflow-hidden';
const menuHeaderStyle = 'py-2 text-lg font-bold text-white';
const menuSubHeaderStyle = 'py-2 text-white';
const menuItemStyle = 'py-2 text-zinc-400 duration-100 hover:text-white';
const socialItemStyle = 'flex bg-white rounded-full overflow-hidden duration-100 group';
const socialItemIconStyle = 'w-8 h-8 fill-main duration-100 group-hover:fill-white';

const Footer = async () => {
    const session = await getServerSession(authOptions);

    const customerPhone = '+358-0-000-000';
    const customerEmail = 'hardwarestore@example.com';
    const cooperationPhone = '+358-0-000-000';
    const cooperationEmail = 'hardwarestore@example.com';
    const address = 'Хельсинки, Сенатская площадь';
    const workingHours = 'пн-вс: 8:00-19:00';

    return (
        <footer className='flex flex-col items-center max-lg:pb-18 bg-zinc-900'>
            <section className='flex w-full justify-center py-8 border-b-1 border-zinc-700'>
                <div className={newsletterStyle}>
                    <div className='flex flex-col justify-center lg:flex-grow'>
                        <h2 className='text-2xl font-bold text-white text-center' children='Подпишитесь на рассылку'/>
                        <p className='text-base text-zinc-400 text-center' children='Узнавайте о новинках и акциях'/>
                    </div>
                    <div className='flex flex-col gap-2 justify-center lg:flex-grow'>
                        <Input type='email' placeholder='Email'/>
                        <Checkbox
                            classNames={{label: 'text-white'}}
                            children='Я даю согласие на обработку персональных данных'
                        />
                    </div>
                    <Button className='lg:flex-grow' children='Отправить'/>
                </div>
            </section>
            <div className='flex w-full justify-center py-8 border-b-1 border-zinc-700'>
                <div className='flex w-full max-w-7xl flex-col md:flex-row items-start justify-center gap-y-8 p-4'>
                    <section className={menuStyle}>
                        <h2 className={menuHeaderStyle} children='О нас'/>
                        {session?.user?.role === Role.ADMIN &&
                            <Link className={menuItemStyle} href='/admin' children='Управление'/>
                        }
                        <Link className={menuItemStyle} href='/contacts' children='Контакты'/>
                        <Link className={menuItemStyle} href='/about' children='О компании'/>
                    </section>
                    <section className={menuStyle}>
                        <h2 className={menuHeaderStyle} children='Покупателям'/>
                        <Link className={menuItemStyle} href='/' children='Главная'/>
                        <Link className={menuItemStyle} href='/catalog' children='Каталог'/>
                        <Link className={menuItemStyle} href='/promotions' children='Акции'/>
                        <Link className={menuItemStyle} href='/delivery' children='Доставка'/>
                        <Link className={menuItemStyle} href='/blog' children='Статьи'/>
                    </section>
                    <section className={menuStyle}>
                        <h2 className={menuHeaderStyle} children='Интернет-магазин'/>
                        <div className={menuSubHeaderStyle} children='Для клиентов:'/>
                        <a className={menuItemStyle} href={`tel:${customerPhone}`} children={customerPhone}/>
                        <a className={menuItemStyle} href={`mailto:${customerEmail}`} children={customerEmail}/>
                        <div className={menuSubHeaderStyle} children='Для сотрудничества:'/>
                        <a className={menuItemStyle} href={`tel:${cooperationPhone}`} children={cooperationPhone}/>
                        <a className={menuItemStyle} href={`mailto:${cooperationEmail}`} children={cooperationEmail}/>
                        <div className={menuSubHeaderStyle} children='Адрес и время работы:'/>
                        <Link className={menuItemStyle} href='/contacts' children={address}/>
                        <Link className={menuItemStyle} href='/contacts' children={workingHours}/>
                    </section>
                </div>
            </div>
            <section className='flex w-full justify-center py-8 border-b-1 border-zinc-700'>
                <div className='flex flex-col justify-center gap-4 p-4'>
                    <h2 className='text-xl font-bold text-center text-white' children='Мы в соцсетях'/>
                    <ul className='flex flex-wrap justify-center gap-4'>
                        <li className={`${socialItemStyle} hover:bg-tiktok`}>
                            <Link className='p-2' href='/' aria-label='TikTok'>
                                <TikTokIcon className={socialItemIconStyle}/>
                            </Link>
                        </li>
                        <li className={`${socialItemStyle} hover:bg-youtube`}>
                            <Link className='p-2' href='/' aria-label='YouTube'>
                                <YouTubeIcon className={socialItemIconStyle}/>
                            </Link>
                        </li>
                        <li className={`${socialItemStyle} hover:bg-telegram`}>
                            <Link className='p-2' href='/' aria-label='Telegram'>
                                <TelegramIcon className={socialItemIconStyle}/>
                            </Link>
                        </li>
                        <li className={`${socialItemStyle} hover:bg-viber`}>
                            <Link className='p-2' href='/' aria-label='Viber'>
                                <ViberIcon className={socialItemIconStyle}/>
                            </Link>
                        </li>
                        <li className={`${socialItemStyle} hover:bg-vk`}>
                            <Link className='p-2' href='/' aria-label='Vk'>
                                <VkIcon className={socialItemIconStyle}/>
                            </Link>
                        </li>
                    </ul>
                </div>
            </section>
            <section className='flex w-full justify-center py-8 border-b-1 border-zinc-700'>
                <div className='flex flex-col gap-4 p-4'>
                    <h2 className='text-xl font-bold text-center text-white' children='Мы принимаем к оплате'/>
                    <ul className='flex flex-wrap justify-center gap-4'>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <CashIcon className='w-12 h-12'/>
                        </li>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <VisaIcon className='w-12 h-12'/>
                        </li>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <VisaElectronIcon className='w-12 h-12'/>
                        </li>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <MastercardIcon className='w-12 h-12'/>
                        </li>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <MaestroIcon className='w-12 h-12'/>
                        </li>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <UnionPayIcon className='w-12 h-12'/>
                        </li>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <MirIcon className='w-12 h-12'/>
                        </li>
                        <li className='flex flex-wrap justify-center gap-4'>
                            <SbpIcon className='w-12 h-12'/>
                        </li>
                    </ul>
                </div>
            </section>
            <section className='flex w-full justify-center py-8'>
                <div className='flex w-full max-w-7xl flex-col gap-4 p-4'>
                    <p className='text-zinc-400'>
                        © 2000-{new Date().getFullYear()} Hardware Store.
                    </p>
                    <p className='text-zinc-400'>
                        При полном или частичном использовании материалов с сайта ссылка на источник обязательна.
                    </p>
                    <p className='text-sm text-zinc-400'>
                        Продолжая работу с сайтом, вы даете согласие на использование сайтом cookies и обработку
                        персональных данных в целях функционирования сайта, проведения ретаргетинга, статистических
                        исследований, улучшения сервиса и предоставления релевантной рекламной информации на основе
                        ваших предпочтений и интересов.
                    </p>
                </div>
            </section>
        </footer>
    );
}

export default Footer;
