'use client'

import {Breadcrumbs, BreadcrumbItem} from '@nextui-org/breadcrumbs';
import Link from 'next/link';

interface BreadcrumbRouteItem {
    href: string
    value: string
}

const CustomBreadcrumbs = ({items}: { items: BreadcrumbRouteItem[] }) => {
    return (
        <Breadcrumbs
            className='px-1'
            size='lg'
            separator='/'
            itemClasses={{separator: 'px-2 text-zinc-500'}}
        >
            {items.map((item, index) =>
                <BreadcrumbItem key={item.value}>
                    <Link
                        className={index === items.length - 1 ? 'font-bold text-zinc-700' : 'text-zinc-500'}
                        href={item.href}
                        aria-label={item.value}
                        children={item.value}
                    />
                </BreadcrumbItem>
            )}
        </Breadcrumbs>
    );
}

export default CustomBreadcrumbs;
