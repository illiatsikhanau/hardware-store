'use client'

import {useEffect, useRef, useState} from 'react';
import {useSearchParams} from 'next/navigation';
import onVisible from '@/helpers/onVisible';
import {getProducts, ProductExt, PRODUCTS_PER_PAGE, ProductsQuery} from '@/services/products';
import Spinner from '@/components/Spinner';
import CatalogCard from '@/components/CatalogCard';

const ProductsListAppend = ({query}: { query: ProductsQuery }) => {
    const searchParams = useSearchParams();
    const ref = useRef<HTMLDivElement>(null);
    const [products, setProducts] = useState<ProductExt[]>();

    useEffect(() => {
        setProducts(undefined);

        let observer: IntersectionObserver;
        if (ref.current) {
            observer = onVisible(ref.current, () => {
                getProducts(query).then(products => setProducts(products));
            });
        }
        return () => observer?.disconnect();
    }, [searchParams]);

    return (
        <div className='w-full min-h-unit-12 flex flex-wrap' ref={ref}>
            {products ?
                <>
                    {products.map(product =>
                        <div key={product.id} className='flex w-full sm:w-1/2 xl:w-1/3 p-1'>
                            <CatalogCard product={product}/>
                        </div>
                    )}
                    {products.length === PRODUCTS_PER_PAGE &&
                        <ProductsListAppend query={{
                            ...query,
                            skip: (query.skip ?? 0) + PRODUCTS_PER_PAGE
                        }}/>
                    }
                </>
                :
                <div className='w-full flex justify-center'>
                    <Spinner/>
                </div>
            }
        </div>
    );
}

export default ProductsListAppend;
