import {Chip, ChipProps} from '@nextui-org/chip';

const CustomChip = (props: ChipProps) => {
    return <Chip {...{
        ...props,
        classNames: {
            ...props.classNames,
            content: ['text-white', props.classNames?.content],
        }
    }}/>
}

export default CustomChip;
