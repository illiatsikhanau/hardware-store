'use client'

import {KeyboardEvent, useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveSection, updateActiveSection} from '@/redux/adminReducer';
import {getSections, createSection, updateSection, deleteSection, EMPTY_SECTION, SectionExt} from '@/services/sections';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import Button from '@/components/Button';
import Input from '@/components/Input';
import Spinner from '@/components/Spinner';

const TableSections = () => {
    const dispatch = useAppDispatch();
    const activeSubcategory = useAppSelector(state => state.admin.activeSubcategory);
    const activeSection = useAppSelector(state => state.admin.activeSection);

    const [sections, setSections] = useState<SectionExt[]>();

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitSection = async () => {
        if (activeSection.name) {
            const newSection = activeSection.id ?
                await updateSection(activeSection) :
                await createSection(activeSection);

            if (sections && newSection) {
                await updateSections().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitSection();
        }
    }

    const removeSection = async () => {
        const success = await deleteSection(activeSection.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateSections().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectSection = (section: SectionExt) => {
        dispatch(setActiveSection(section));
        onOpen();
    }

    const updateSections = async () => {
        if (activeSubcategory.id) {
            const sections = await getSections({subcategoryId: `equals:${activeSubcategory.id}`});
            setSections(sections);
        } else {
            setSections([]);
        }

    }

    useEffect(() => {
        void updateSections();
    }, [activeSubcategory]);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveSection(EMPTY_SECTION));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='w-full flex flex-col gap-2'>
            {sections ?
                <>
                    {activeSubcategory.id > 0 &&
                        <Button
                            className='w-fit bg-green'
                            onClick={onOpen}
                            children='Добавить'
                        />
                    }
                    <Table aria-label='Подразделы' removeWrapper hideHeader={!sections.length}>
                        <TableHeader>
                            <TableColumn className='!rounded-md bg-zinc-800 text-white' children='Название'/>
                        </TableHeader>
                        <TableBody emptyContent='Нет подразделов'>
                            {sections.map(section =>
                                <TableRow
                                    key={section.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectSection(section)}
                                >
                                    <TableCell className='rounded-md' children={section.name}/>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeSection.id ? 'Редактирование' : 'Добавление'} подраздела
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название подраздела'
                            value={activeSection.name}
                            onChange={e => dispatch(updateActiveSection(['name', e.target.value]))}
                            autoFocus
                        />
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeSection.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить подраздел "${activeSection.name}"?`}
                                    onConfirm={removeSection}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitSection}
                            isDisabled={!activeSection.name}
                            children={activeSection.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableSections;
