import {BenefitExt} from '@/services/benefits';
import Link from 'next/link';
import Image from 'next/image';

const wrapperStyle = [
    'flex w-full h-16 items-center gap-2 p-2 rounded-md border-1 border-zinc-200 bg-zinc-100',
    'duration-100 hover:bg-zinc-800 cursor-pointer group'
].join(' ');

const BenefitCard = ({benefit}: { benefit: BenefitExt }) => {
    const content = (
        <>
            <div
                className='flex w-12 h-12 items-center duration-100 group-hover:brightness-0 group-hover:invert'>
                <Image src={benefit.icon} alt='Изображение' width={48} height={48}/>
            </div>
            <b className='w-full text-center duration-100 group-hover:text-white' children={benefit.name}/>
        </>
    );

    return (
        <>
            {benefit.href ?
                <Link className={wrapperStyle} href={benefit.href} aria-label={benefit.href} children={content}/> :
                <div className={wrapperStyle} children={content}/>
            }
        </>
    );
}

export default BenefitCard;
