'use client'

import {KeyboardEvent, useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveSubcategory, updateActiveSubcategory} from '@/redux/adminReducer';
import {
    getSubcategories, createSubcategory, updateSubcategory, deleteSubcategory,
    EMPTY_SUBCATEGORY, SubcategoryExt
} from '@/services/subcategories';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import Button from '@/components/Button';
import Input from '@/components/Input';
import Spinner from '@/components/Spinner';

const TableSubcategories = () => {
    const dispatch = useAppDispatch();
    const activeCategory = useAppSelector(state => state.admin.activeCategory);
    const activeSubcategory = useAppSelector(state => state.admin.activeSubcategory);

    const [subcategories, setSubcategories] = useState<SubcategoryExt[]>();

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitSubcategory = async () => {
        if (activeSubcategory.name) {
            const newSubcategory = activeSubcategory.id ?
                await updateSubcategory(activeSubcategory) :
                await createSubcategory(activeSubcategory);

            if (subcategories && newSubcategory) {
                await updateSubcategories().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitSubcategory();
        }
    }

    const removeSubcategory = async () => {
        const success = await deleteSubcategory(activeSubcategory.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateSubcategories().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectSubcategory = (subcategory: SubcategoryExt) => {
        dispatch(setActiveSubcategory(subcategory));
        onOpen();
    }

    const updateSubcategories = async () => {
        if (activeCategory.id) {
            const subcategories = await getSubcategories({categoryId: `equals:${activeCategory.id}`});
            setSubcategories(subcategories);
        } else {
            setSubcategories([]);
        }
    }

    useEffect(() => {
        void updateSubcategories();
    }, [activeCategory]);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveSubcategory(EMPTY_SUBCATEGORY));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='w-full flex flex-col gap-2'>
            {subcategories ?
                <>
                    {activeCategory.id > 0 &&
                        <Button
                            className='w-fit bg-green'
                            onClick={onOpen}
                            children='Добавить'
                        />
                    }
                    <Table aria-label='Подкатегории' removeWrapper hideHeader={!subcategories.length}>
                        <TableHeader>
                            <TableColumn className='!rounded-md bg-zinc-800 text-white' children='Название'/>
                        </TableHeader>
                        <TableBody emptyContent='Нет подкатегорий'>
                            {subcategories.map(subcategory =>
                                <TableRow
                                    key={subcategory.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectSubcategory(subcategory)}
                                >
                                    <TableCell className='rounded-md' children={subcategory.name}/>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeSubcategory.id ? 'Редактирование' : 'Добавление'} подкатегории
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название подкатегории'
                            value={activeSubcategory.name}
                            onChange={e => dispatch(updateActiveSubcategory(['name', e.target.value]))}
                            autoFocus
                        />
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeSubcategory.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить подкатегорию "${activeSubcategory.name}"?`}
                                    onConfirm={removeSubcategory}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitSubcategory}
                            isDisabled={!activeSubcategory.name}
                            children={activeSubcategory.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableSubcategories;
