'use client'

import {useAppSelector} from '@/redux/hooks';

const BadgeCounter = ({type}: { type: 'favorite' | 'cart' }) => {
    const favoriteCount = useAppSelector(state => state.user.favoriteCount);
    const count = type === 'favorite' ? favoriteCount : 19;

    const wrapperStyle = [
        'relative -top-2 -left-1 w-7 duration-100 group-hover:-top-3',
        count > 0 ? '' : 'opacity-0'
    ].join(' ');

    return (
        <div className={wrapperStyle}>
            <span
                children={count}
                className='w-fit min-w-5 flex items-center h-5 py-1 px-1.5 rounded-full text-sm bg-main text-white'
            />
        </div>
    );
}

export default BadgeCounter;
