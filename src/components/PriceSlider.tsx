'use client'

import {Slider, SliderValue} from '@nextui-org/slider';

interface PriceSliderProps {
    minPrice: number
    maxPrice: number
    price: number[]
    setPrice: (value: number[]) => void
}

const PriceSlider = (props: PriceSliderProps) => {
    const onPriceChange = (value: SliderValue) => {
        const price = value as number[];
        props.setPrice(price);
    };

    return (
        <Slider
            classNames={{
                track: 'bg-zinc-300',
                filler: 'bg-accent',
                thumb: 'bg-accent',
                label: 'text-medium text-zinc-700'
            }}
            size='sm'
            label='Цена'
            formatOptions={{style: 'currency', currency: 'EUR'}}
            minValue={props.minPrice}
            maxValue={props.maxPrice}
            step={0.01}
            value={props.price}
            onChange={value => onPriceChange(value)}
        />
    );
}

export default PriceSlider;
