'use client'

import {KeyboardEvent, useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveProduct, updateActiveProduct} from '@/redux/adminReducer';
import {
    getProducts, getProductsInfo, createProduct, updateProduct, deleteProduct,
    ProductExt, EMPTY_PRODUCT, PRODUCTS_PER_PAGE,
} from '@/services/products';
import {ProductUnitExt} from '@/services/productUnits';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import Button from '@/components/Button';
import Input from '@/components/Input';
import Image from 'next/image';
import FileUpload from '@/components/FileUpload';
import {Tooltip} from '@nextui-org/tooltip';
import {Select, SelectItem} from '@nextui-org/select';
import Spinner from '@/components/Spinner';
import Pagination from '@/components/Pagination';
import TableSearch from '@/components/TableSearch';

const helpStyle = 'w-5 h-5 flex items-center justify-center rounded-full bg-zinc-700 text-white cursor-pointer';

const TableProducts = ({productUnits}: { productUnits: ProductUnitExt[] }) => {
    const dispatch = useAppDispatch();
    const activeProduct = useAppSelector(state => state.admin.activeProduct);
    const activeSection = useAppSelector(state => state.admin.activeSection);

    const [products, setProducts] = useState<ProductExt[]>();
    const [page, setPage] = useState(1);
    const [pages, setPages] = useState(1);
    const [search, setSearch] = useState('');

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitProduct = async () => {
        const formattedActiveProduct = {
            ...activeProduct,
            count: Number(activeProduct.count),
            price: Number(activeProduct.price),
        }

        if (
            formattedActiveProduct.name &&
            formattedActiveProduct.src &&
            formattedActiveProduct.count >= 0 &&
            formattedActiveProduct.price >= 0
        ) {
            const newProduct = activeProduct.id ?
                await updateProduct(formattedActiveProduct) :
                await createProduct(formattedActiveProduct);

            if (products && newProduct) {
                await updateProducts().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitProduct();
        }
    }

    const removeProduct = async () => {
        const success = await deleteProduct(activeProduct.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateProducts().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectProduct = (product: ProductExt) => {
        dispatch(setActiveProduct(product));
        onOpen();
    }

    const updateProducts = async () => {
        if (activeSection.id) {
            const query = {
                name: `contains:${search}`,
                sectionId: `equals:${activeSection.id}`,
                skip: (page - 1) * PRODUCTS_PER_PAGE,
            }
            const info = await getProductsInfo(query);
            const products = await getProducts(query);
            setPages(Math.ceil(info.count / PRODUCTS_PER_PAGE))
            setProducts(products);
        } else {
            setPages(1);
            setProducts([]);
        }
    }

    useEffect(() => {
        void updateProducts();
    }, [activeSection, page]);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveProduct(EMPTY_PRODUCT));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='w-full flex flex-col gap-2'>
            {products ?
                <>
                    {activeSection.id > 0 &&
                        <div className='flex max-lg:flex-col items-center justify-between gap-y-2'>
                            <div className='flex gap-2'>
                                <TableSearch
                                    search={search}
                                    setSearch={setSearch}
                                    onSearch={updateProducts}
                                />
                                <Button
                                    className='min-w-unit-24 bg-green'
                                    onClick={onOpen}
                                    children='Добавить'
                                />
                            </div>
                            {(pages > 1 || page > 1) &&
                                <Pagination total={pages} page={page} onChange={page => setPage(page)}/>
                            }
                        </div>
                    }
                    <Table aria-label='Товары' removeWrapper hideHeader={!products.length}>
                        <TableHeader>
                            <TableColumn
                                className='!rounded-l-md bg-zinc-800 text-white'
                                children='Название'
                            />
                            <TableColumn
                                className='!rounded-r-md bg-zinc-800 text-white text-center'
                                children='Картинка'
                            />
                        </TableHeader>
                        <TableBody emptyContent='Нет товаров'>
                            {products.map(product =>
                                <TableRow
                                    key={product.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectProduct({...product, unit: product.unit})}
                                >
                                    <TableCell
                                        className='rounded-l-md'
                                        children={product.name}
                                    />
                                    <TableCell className='rounded-r-md'>
                                        <Image
                                            className='w-full h-14 object-scale-down'
                                            src={product.src}
                                            alt='Изображение'
                                            width={48}
                                            height={48}
                                        />
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeProduct.id ? 'Редактирование' : 'Добавление'} товара
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название товара'
                            value={activeProduct.name}
                            onChange={e => dispatch(updateActiveProduct(['name', e.target.value]))}
                            autoFocus
                        />
                        <div className='flex gap-2'>
                            <FileUpload
                                content='Выберите картинку'
                                defaultContent={activeProduct.src ? 'Текущая картинка' : null}
                                onChangeFileUrl={url => dispatch(updateActiveProduct(['src', url]))}
                            />
                            <div className='hidden md:flex items-center'>
                                <Tooltip placement='top' content='Рекомендуемый формат - jpg/webp 300x150 пикселей'>
                                    <span className={helpStyle} children='?'/>
                                </Tooltip>
                            </div>
                        </div>
                        <div className='flex max-s:flex-col gap-4'>
                            <Input
                                startContent={
                                    <span className='text-sm text-zinc-400 whitespace-nowrap' children='Количество:'/>
                                }
                                type='number'
                                value={activeProduct.count.toString()}
                                onChange={e => dispatch(updateActiveProduct(['count', e.target.value]))}
                                onBlur={() => dispatch(updateActiveProduct(['count', Number(activeProduct.count)]))}
                            />
                            <Select
                                classNames={{
                                    trigger: 'h-10 rounded-md border-1 border-zinc-200 bg-zinc-100 !duration-100',
                                    selectorIcon: 'stroke-zinc-700',
                                    value: '!text-zinc-700',
                                }}
                                listboxProps={{itemClasses: {base: 'rounded-md'}}}
                                popoverProps={{classNames: {content: 'p-1 rounded-md'}}}
                                placeholder='Выберите единицу товара'
                                selectedKeys={activeProduct.unitId ? [activeProduct.unitId.toString()] : []}
                                onChange={e => dispatch(updateActiveProduct(
                                    ['unitId', e.target.value ? Number(e.target.value) : null]
                                ))}
                                aria-label='Роль'
                            >
                                {productUnits.map(productUnit =>
                                    <SelectItem key={productUnit.id} children={productUnit.name}/>
                                )}
                            </Select>
                        </div>
                        <Input
                            startContent={
                                <span className='text-sm text-zinc-400 whitespace-nowrap' children='Цена:'/>
                            }
                            type='number'
                            value={activeProduct.price.toString()}
                            onChange={e => dispatch(updateActiveProduct(['price', e.target.value]))}
                            onBlur={() => dispatch(updateActiveProduct(['price', Number(activeProduct.price)]))}
                        />
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeProduct.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить товар '${activeProduct.name}'?`}
                                    onConfirm={removeProduct}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitProduct}
                            isDisabled={
                                !activeProduct.name ||
                                !activeProduct.src ||
                                activeProduct.count < 0 ||
                                activeProduct.price < 0
                            }
                            children={activeProduct.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableProducts;
