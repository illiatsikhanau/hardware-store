'use client'

import {useEffect, useState, KeyboardEvent} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveCategory, updateActiveCategory} from '@/redux/adminReducer';
import {
    getCategories, createCategory, updateCategory, deleteCategory,
    CategoryExt, EMPTY_CATEGORY
} from '@/services/categories';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableHeader, TableBody, TableColumn, TableRow, TableCell} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import {Tooltip} from '@nextui-org/tooltip';
import Image from 'next/image';
import Spinner from '@/components/Spinner';
import Button from '@/components/Button';
import Input from '@/components/Input';
import FileUpload from '@/components/FileUpload';

const helpStyle = 'w-5 h-5 flex items-center justify-center rounded-full bg-zinc-700 text-white cursor-pointer';

const TableCategories = () => {
    const dispatch = useAppDispatch();
    const activeCategory = useAppSelector(state => state.admin.activeCategory);

    const [categories, setCategories] = useState<CategoryExt[]>();

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitCategory = async () => {
        if (activeCategory.name && activeCategory.src && activeCategory.icon) {
            const newCategory = activeCategory.id ?
                await updateCategory(activeCategory) :
                await createCategory(activeCategory);

            if (categories && newCategory) {
                await updateCategories().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitCategory();
        }
    }

    const removeCategory = async () => {
        const success = await deleteCategory(activeCategory.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateCategories().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectCategory = (category: CategoryExt) => {
        dispatch(setActiveCategory(category));
        onOpen();
    }

    const updateCategories = async () => {
        const categories = await getCategories();
        setCategories(categories);
    }

    useEffect(() => {
        void updateCategories();
    }, []);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveCategory(EMPTY_CATEGORY));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='flex flex-col gap-2'>
            {categories ?
                <>
                    <Button
                        className='w-fit bg-green'
                        onClick={onOpen}
                        children='Добавить'
                    />
                    <Table aria-label='Категории' removeWrapper hideHeader={!categories.length}>
                        <TableHeader>
                            <TableColumn className='!rounded-l-md bg-zinc-800 text-white' children='Название'/>
                            <TableColumn className='bg-zinc-800 text-white' children='Картинка'/>
                            <TableColumn className='!rounded-r-md bg-zinc-800 text-white' children='Иконка'/>
                        </TableHeader>
                        <TableBody emptyContent='Нет категорий'>
                            {categories.map(category =>
                                <TableRow
                                    key={category.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectCategory(category)}
                                >
                                    <TableCell className='rounded-l-md' children={category.name}/>
                                    <TableCell>
                                        <Image src={category.src} alt='Изображение' width={24} height={24}/>
                                    </TableCell>
                                    <TableCell className='rounded-r-md'>
                                        <Image src={category.icon} alt='Изображение' width={24} height={24}/>
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeCategory.id ? 'Редактирование' : 'Добавление'} категории
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название категории'
                            value={activeCategory.name}
                            onChange={e => dispatch(updateActiveCategory(['name', e.target.value]))}
                            autoFocus
                        />
                        <div className='flex gap-2'>
                            <FileUpload
                                content='Выберите картинку'
                                defaultContent={activeCategory.src ? 'Текущая картинка' : null}
                                onChangeFileUrl={url => dispatch(updateActiveCategory(['src', url]))}
                            />
                            <div className='hidden md:flex items-center'>
                                <Tooltip placement='top' content='Рекомендуемый формат - jpg/webp 128x128 пикселей'>
                                    <span className={helpStyle} children='?'/>
                                </Tooltip>
                            </div>
                        </div>
                        <div className='flex gap-2'>
                            <FileUpload
                                content='Выберите иконку'
                                defaultContent={activeCategory.icon ? 'Текущая иконка' : null}
                                onChangeFileUrl={url => dispatch(updateActiveCategory(['icon', url]))}
                            />
                            <div className='hidden md:flex items-center'>
                                <Tooltip placement='top' content='Рекомендуемый формат - svg'>
                                    <span className={helpStyle} children='?'/>
                                </Tooltip>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeCategory.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить категорию "${activeCategory.name}"?`}
                                    onConfirm={removeCategory}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitCategory}
                            isDisabled={!activeCategory.name || !activeCategory.src || !activeCategory.icon}
                            children={activeCategory.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableCategories;
