import {Checkbox, CheckboxProps} from '@nextui-org/checkbox';

const CustomCheckbox = (props: CheckboxProps) => {
    return <Checkbox {...{
        ...props,
        classNames: {
            ...props.classNames,
            base: ['p-0 m-0', props.classNames?.base],
            wrapper: ['after:bg-accent after:border-2', props.classNames?.wrapper],
            label: ['text-inherit', props.classNames?.label]
        }
    }}/>
}

export default CustomCheckbox;
