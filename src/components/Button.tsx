import {Button, ButtonProps} from '@nextui-org/button';

const CustomButton = (props: ButtonProps) => {
    return <Button {...{
        ...props,
        className: `rounded-md text-base bg-accent text-white 
        !duration-100 data-[hover=true]:opacity-90 ${props.className ?? ''}`,
    }}/>
}

export default CustomButton;
