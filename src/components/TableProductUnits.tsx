'use client'

import {KeyboardEvent, useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActiveProductUnit, updateActiveProductUnit} from '@/redux/adminReducer';
import {
    getProductUnits, createProductUnit, updateProductUnit, deleteProductUnit,
    ProductUnitExt, EMPTY_PRODUCT_UNIT
} from '@/services/productUnits';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import Button from '@/components/Button';
import Input from '@/components/Input';

interface TableProductUnitsProps {
    productUnits: ProductUnitExt[],
    setProductUnits: (productUnits: ProductUnitExt[]) => void
}

const TableProductUnits = (props: TableProductUnitsProps) => {
    const dispatch = useAppDispatch();
    const activeProductUnit = useAppSelector(state => state.admin.activeProductUnit);

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitProductUnit = async () => {
        if (activeProductUnit.name) {
            const newProductUnit = activeProductUnit.id ?
                await updateProductUnit(activeProductUnit) :
                await createProductUnit(activeProductUnit);

            if (newProductUnit) {
                await updateProductUnits().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitProductUnit();
        }
    }

    const removeProductUnit = async () => {
        const success = await deleteProductUnit(activeProductUnit.id);
        toggleDeleteModalVisible();

        if (success) {
            await updateProductUnits().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectProductUnit = (productUnit: ProductUnitExt) => {
        dispatch(setActiveProductUnit(productUnit));
        onOpen();
    }

    const updateProductUnits = async () => {
        const productUnits = await getProductUnits();
        props.setProductUnits(productUnits);
    }

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActiveProductUnit(EMPTY_PRODUCT_UNIT));
            setError('');
        }
    }, [isOpen]);

    return (
        <div className='w-full flex flex-col gap-2'>
            <Button
                className='w-fit bg-green'
                onClick={onOpen}
                children='Добавить'
            />
            <Table aria-label='Единицы товаров' removeWrapper hideHeader={!props.productUnits.length}>
                <TableHeader>
                    <TableColumn className='!rounded-md bg-zinc-800 text-white' children='Название'/>
                </TableHeader>
                <TableBody emptyContent='Нет единиц товаров'>
                    {props.productUnits.map(productUnit =>
                        <TableRow
                            key={productUnit.id}
                            className='cursor-pointer duration-100 hover:bg-zinc-200'
                            onClick={() => selectProductUnit(productUnit)}
                        >
                            <TableCell className='rounded-md' children={productUnit.name}/>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activeProductUnit.id ? 'Редактирование' : 'Добавление'} единицы товара
                    </ModalHeader>
                    <ModalBody className='gap-4 p-0'>
                        {error &&
                            <p className='text-center text-red font-medium' children={error}/>
                        }
                        <Input
                            placeholder='Название единицы товара'
                            value={activeProductUnit.name}
                            onChange={e => dispatch(updateActiveProductUnit(['name', e.target.value]))}
                            autoFocus
                        />
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activeProductUnit.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content={`Вы действительно хотите удалить единицу товара "${activeProductUnit.name}"?`}
                                    onConfirm={removeProductUnit}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitProductUnit}
                            isDisabled={!activeProductUnit.name}
                            children={activeProductUnit.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TableProductUnits;
