'use client'

import {useEffect, useState, KeyboardEvent} from 'react';
import {useAppDispatch, useAppSelector} from '@/redux/hooks';
import {setActivePromotion, updateActivePromotion} from '@/redux/adminReducer';
import {
    getPromotions, getPromotionsInfo, updatePromotion, createPromotion, deletePromotion,
    EMPTY_PROMOTION, PROMOTIONS_PER_PAGE, PromotionExt,
} from '@/services/promotions';
import {getProducts, getProductsInfo, ProductExt} from '@/services/products';
import {NOT_FILLED_ERROR, SERVER_ERROR} from '@/helpers/errors';
import {getDateString, isEqualDates} from '@/helpers/date';
import {COLORS, getHexColor} from '@/helpers/color';
import {Table, TableBody, TableCell, TableColumn, TableHeader, TableRow} from '@nextui-org/table';
import {Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure} from '@nextui-org/modal';
import ModalDeleteConfirmation from '@/components/ModalDeleteConfirmation';
import {Tab, Tabs} from '@nextui-org/tabs';
import {Github} from '@uiw/react-color';
import Button from '@/components/Button';
import Spinner from '@/components/Spinner';
import Input from '@/components/Input';
import DatePicker from '@/components/DatePicker';
import TableSearch from '@/components/TableSearch';
import Pagination from '@/components/Pagination';
import Checkbox from '@/components/Checkbox';
import Chip from '@/components/Chip';

const PRODUCTS_PER_PAGE = 15;

const TablePromotions = () => {
    const dispatch = useAppDispatch();
    const activePromotion = useAppSelector(state => state.admin.activePromotion);
    const hasProduct = (id: number) => !!activePromotion.products.find(product => product.id === id);
    const toggleProduct = (id: number) => {
        const idSet = new Set(activePromotion.products.map(product => product.id));
        idSet.has(id) ? idSet.delete(id) : idSet.add(id);
        dispatch(updateActivePromotion([
            'products',
            [...idSet].map(id => {
                return {id}
            })
        ]));
    }
    const toggleAllProducts = (checked: boolean) => checked ? addAllProducts() : deleteAllProducts();
    const addAllProducts = () => {
        const idSet = new Set(activePromotion.products.map(product => product.id));
        products.forEach(product => idSet.add(product.id));
        dispatch(updateActivePromotion([
            'products',
            [...idSet].map(id => {
                return {id}
            })
        ]));
    }
    const deleteAllProducts = () => {
        dispatch(updateActivePromotion([
            'products',
            activePromotion.products.filter(p => !products.find(product => product.id === p.id))
        ]));
    }

    const [promotions, setPromotions] = useState<PromotionExt[]>();
    const [page, setPage] = useState(1);
    const [pages, setPages] = useState(1);
    const [search, setSearch] = useState('');

    const [products, setProducts] = useState<ProductExt[]>([]);
    const [productsPage, setProductsPage] = useState(1);
    const [productsPages, setProductsPages] = useState(1);
    const [productsSearch, setProductsSearch] = useState('');
    const hasUnselectedProduct = !products.find(product => !activePromotion.products.find(p => p.id === product.id));

    const [error, setError] = useState('');
    const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
    const toggleDeleteModalVisible = () => setIsDeleteModalVisible(!isDeleteModalVisible);

    const {isOpen, onOpen, onClose} = useDisclosure();

    const submitPromotion = async () => {
        const formattedActivePromotion = {
            ...activePromotion,
            discount: Number(activePromotion.discount),
            from: Number(activePromotion.from),
            every: Number(activePromotion.every),
        }

        if (
            formattedActivePromotion.name &&
            formattedActivePromotion.chipName &&
            formattedActivePromotion.discount >= 0 &&
            formattedActivePromotion.every >= 1 &&
            formattedActivePromotion.from >= 1
        ) {
            const newPromotion = activePromotion.id ?
                await updatePromotion(formattedActivePromotion) :
                await createPromotion(formattedActivePromotion);

            if (promotions && newPromotion) {
                await updatePromotions().then(onClose);
            } else {
                setError(SERVER_ERROR);
            }
        } else {
            setError(NOT_FILLED_ERROR);
        }
    }

    const onModalKeyDown = (e: KeyboardEvent<HTMLElement>) => {
        if (!isDeleteModalVisible && e.key === 'Enter') {
            void submitPromotion();
        }
    }

    const removePromotion = async () => {
        const success = await deletePromotion(activePromotion.id);
        toggleDeleteModalVisible();

        if (success) {
            await updatePromotions().then(onClose);
        } else {
            setError(SERVER_ERROR);
        }
    }

    const selectPromotion = (promotion: PromotionExt) => {
        dispatch(setActivePromotion(promotion));
        onOpen();
    }

    const updatePromotions = async () => {
        const query = {
            name: `contains:${search}`,
            skip: (page - 1) * PROMOTIONS_PER_PAGE,
        }
        const info = await getPromotionsInfo(query);
        const promotions = await getPromotions(query);
        setPages(Math.ceil(info.count / PROMOTIONS_PER_PAGE));
        setPromotions(promotions);
    }

    const updateProducts = async () => {
        if (productsSearch) {
            const query = {
                name: `contains:${productsSearch}`,
                skip: (productsPage - 1) * PRODUCTS_PER_PAGE,
                take: PRODUCTS_PER_PAGE,
            }
            const info = await getProductsInfo(query);
            const products = await getProducts(query);
            setProductsPages(Math.ceil(info.count / PRODUCTS_PER_PAGE));
            setProducts(products);
        } else {
            setProductsPages(1);
            setProducts([]);
        }
    }

    useEffect(() => {
        void updatePromotions();
    }, [page]);

    useEffect(() => {
        void updateProducts();
    }, [productsPage]);

    useEffect(() => {
        if (!isOpen) {
            dispatch(setActivePromotion(EMPTY_PROMOTION));
            setError('');

            setProducts([]);
            setProductsPage(1);
            setProductsPages(1);
            setProductsSearch('');
        }
    }, [isOpen]);

    return (
        <div className='flex flex-col gap-2'>
            {promotions ?
                <>
                    <div className='flex max-lg:flex-col items-center justify-between gap-y-2'>
                        <div className='flex gap-2'>
                            <TableSearch
                                search={search}
                                setSearch={setSearch}
                                onSearch={updatePromotions}
                            />
                            <Button
                                className='min-w-unit-24 bg-green'
                                onClick={onOpen}
                                children='Добавить'
                            />
                        </div>
                        {(pages > 1 || page > 1) &&
                            <Pagination total={pages} page={page} onChange={page => setPage(page)}/>
                        }
                    </div>
                    <Table aria-label='Акции' removeWrapper hideHeader={!promotions.length}>
                        <TableHeader>
                            <TableColumn
                                className='!rounded-l-md bg-zinc-800 text-white'
                                children='Название'
                            />
                            <TableColumn
                                className='!rounded-r-md bg-zinc-800 text-white'
                                children='Сроки акции'
                            />
                        </TableHeader>
                        <TableBody emptyContent='Нет акций'>
                            {promotions.map(promotion =>
                                <TableRow
                                    key={promotion.id}
                                    className='cursor-pointer duration-100 hover:bg-zinc-200'
                                    onClick={() => selectPromotion(promotion)}
                                >
                                    <TableCell className='rounded-l-md' children={promotion.name}/>
                                    <TableCell className='rounded-r-md'>
                                        {promotion.startsAt &&
                                            <>
                                                <span className='whitespace-nowrap'>
                                                    с {getDateString(promotion.startsAt)}
                                                </span>
                                                <br/>
                                            </>
                                        }
                                        {promotion.expiresAt &&
                                            <span className='whitespace-nowrap'>
                                                до {getDateString(promotion.expiresAt)}
                                            </span>
                                        }
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </> :
                <Spinner/>
            }
            <Modal
                hideCloseButton
                placement='top-center'
                isOpen={isOpen}
                onClose={onClose}
                onKeyDown={onModalKeyDown}
                size='xl'
            >
                <ModalContent className='flex gap-6 p-6 bg-white'>
                    <ModalHeader className='p-0 text-zinc-700'>
                        {activePromotion.id ? 'Редактирование' : 'Добавление'} акции
                    </ModalHeader>
                    <ModalBody className='p-0'>
                        <Tabs
                            classNames={{
                                base: 'w-full rounded-md bg-zinc-800',
                                tabList: 'bg-transparent',
                                cursor: 'rounded-md bg-white',
                                tab: 'group data-[hover-unselected=true]:opacity-80',
                                tabContent: 'p-3 text-white group-data-[selected=true]:text-zinc-700'
                            }}
                            aria-label='Условия акции'
                        >
                            <Tab className='w-full flex flex-col gap-4 p-0' title='Условия'>
                                {error &&
                                    <p className='text-center text-red font-medium' children={error}/>
                                }
                                <Input
                                    placeholder='Название акции'
                                    value={activePromotion.name}
                                    onChange={e => dispatch(updateActivePromotion(['name', e.target.value]))}
                                    autoFocus
                                />
                                <div className='flex max-s:flex-col gap-4'>
                                    <Input
                                        placeholder='Чип'
                                        value={activePromotion.chipName}
                                        onChange={e => dispatch(updateActivePromotion(['chipName', e.target.value]))}
                                    />
                                    <Input
                                        startContent={
                                            <span
                                                className='text-sm text-zinc-400 whitespace-nowrap'
                                                children='Скидка:'
                                            />
                                        }
                                        endContent={
                                            <span className='text-sm text-zinc-400 whitespace-nowrap' children='%'/>
                                        }
                                        type='number'
                                        value={activePromotion.discount.toString()}
                                        onChange={e => dispatch(updateActivePromotion(['discount', e.target.value]))}
                                        onBlur={() => dispatch(updateActivePromotion([
                                            'discount',
                                            Number(activePromotion.discount)
                                        ]))}
                                    />
                                </div>
                                <div className='flex max-s:flex-col gap-4'>
                                    <Input
                                        startContent={
                                            <span
                                                className='text-sm text-zinc-400 whitespace-nowrap'
                                                children='На каждый:'
                                            />
                                        }
                                        type='number'
                                        value={activePromotion.every.toString()}
                                        onChange={e => dispatch(updateActivePromotion(['every', e.target.value]))}
                                        onBlur={() => dispatch(updateActivePromotion([
                                            'every',
                                            Number(activePromotion.every)
                                        ]))}
                                    />
                                    <Input
                                        startContent={
                                            <span
                                                className='text-sm text-zinc-400 whitespace-nowrap'
                                                children='Начиная с:'
                                            />
                                        }
                                        type='number'
                                        value={activePromotion.from.toString()}
                                        onChange={e => dispatch(updateActivePromotion(['from', e.target.value]))}
                                        onBlur={() => dispatch(updateActivePromotion([
                                            'from',
                                            Number(activePromotion.from)
                                        ]))}
                                    />
                                </div>
                                <div className='flex max-sm:flex-col gap-4 justify-center'>
                                    <div className='flex flex-col items-center gap-1'>
                                        <p className='text-zinc-700'>
                                            Дата начала: <b>{getDateString(activePromotion.startsAt) || 'нет'}</b>
                                        </p>
                                        <DatePicker
                                            selected={activePromotion.startsAt ?
                                                new Date(activePromotion.startsAt?.toString())
                                                : null
                                            }
                                            onSelect={date => {
                                                dispatch(updateActivePromotion([
                                                    'startsAt',
                                                    isEqualDates(activePromotion.startsAt, date) ? date : null
                                                ]))
                                            }}
                                            onChange={() => {
                                            }}
                                            minDate={new Date()}
                                        />
                                    </div>
                                    <div className='flex flex-col items-center gap-1'>
                                        <p className='text-zinc-700'>
                                            Дата окончания: <b>{getDateString(activePromotion.expiresAt) || 'нет'}</b>
                                        </p>
                                        <DatePicker
                                            selected={activePromotion.expiresAt ?
                                                new Date(activePromotion.expiresAt?.toString())
                                                : null
                                            }
                                            onSelect={date => {
                                                dispatch(updateActivePromotion([
                                                    'expiresAt',
                                                    isEqualDates(activePromotion.expiresAt, date) ? date : null
                                                ]))
                                            }}
                                            onChange={() => {
                                            }}
                                            minDate={new Date()}
                                        />
                                    </div>
                                </div>
                                <Github
                                    color={getHexColor(activePromotion.chipColor)}
                                    colors={COLORS}
                                    onChange={color => dispatch(updateActivePromotion([
                                        'chipColor',
                                        parseInt(color.hex.replace('#', ''), 16)
                                    ]))}
                                />
                                <b>Предпросмотр</b>
                                <p>
                                    <Chip
                                        classNames={{
                                            base: [
                                                'uppercase z-20 opacity-90',
                                                `bg-[${getHexColor(activePromotion.chipColor)}]`
                                            ]
                                        }}
                                        children={activePromotion.chipName}
                                    />
                                    &nbsp;
                                    Скидка <b>{activePromotion.discount}%</b> на каждый {activePromotion.every > 1 ?
                                    <b>{activePromotion.every}-й</b> : ''} акционный товар в
                                    корзине {activePromotion.from > 1 ?
                                    <b>начиная с {activePromotion.from} товара</b> : ''}
                                </p>
                            </Tab>
                            <Tab className='w-full flex flex-col gap-4 p-0' title='Товары'>
                                <TableSearch
                                    search={productsSearch}
                                    setSearch={setProductsSearch}
                                    onSearch={updateProducts}
                                />
                                {(productsPages > 1 || productsPage > 1) &&
                                    <Pagination
                                        classNames={{base: 'flex justify-center'}}
                                        total={productsPages}
                                        page={productsPage}
                                        onChange={page => setProductsPage(page)}
                                    />
                                }
                                <p>
                                    Всего товаров в акции: <b>{activePromotion.products.length}</b>
                                    &nbsp;
                                    <span
                                        className='text-accent cursor-pointer duration-100 hover:opacity-80'
                                        onClick={() => dispatch(updateActivePromotion(['products', []]))}
                                        children='удалить все'
                                    />
                                </p>
                                <Table aria-label='Товары' removeWrapper hideHeader={!products.length}>
                                    <TableHeader>
                                        <TableColumn className='!rounded-l-md bg-zinc-800 text-white'>
                                            <Checkbox
                                                isSelected={hasUnselectedProduct}
                                                onValueChange={toggleAllProducts}
                                            />
                                        </TableColumn>
                                        <TableColumn
                                            className='!rounded-r-md bg-zinc-800 text-white'
                                            children='Название товара'
                                        />
                                    </TableHeader>
                                    <TableBody emptyContent='Нет товаров'>
                                        {products.map(product =>
                                            <TableRow
                                                key={product.id}
                                                className='cursor-pointer duration-100 hover:bg-zinc-200'
                                            >
                                                <TableCell
                                                    className='rounded-l-md'
                                                    onClick={() => toggleProduct(product.id)}
                                                >
                                                    <Checkbox
                                                        isSelected={hasProduct(product.id)}
                                                        onValueChange={() => toggleProduct(product.id)}
                                                    />
                                                </TableCell>
                                                <TableCell
                                                    className='rounded-r-md select-none'
                                                    children={product.name}
                                                    onClick={() => toggleProduct(product.id)}
                                                />
                                            </TableRow>
                                        )}
                                    </TableBody>
                                </Table>
                            </Tab>
                        </Tabs>
                    </ModalBody>
                    <ModalFooter className='p-0'>
                        <Button
                            className='bg-transparent text-main border-2 border-main'
                            onPress={onClose}
                            children='Отмена'
                        />
                        {activePromotion.id > 0 &&
                            <>
                                <Button className='bg-main' onClick={toggleDeleteModalVisible} children='Удалить'/>
                                <ModalDeleteConfirmation
                                    isVisible={isDeleteModalVisible}
                                    toggleVisible={toggleDeleteModalVisible}
                                    content='Вы действительно хотите удалить акцию'
                                    onConfirm={removePromotion}
                                />
                            </>
                        }
                        <Button
                            className='bg-green'
                            onPress={submitPromotion}
                            isDisabled={
                                !activePromotion.name ||
                                !activePromotion.chipName ||
                                activePromotion.discount < 0 ||
                                activePromotion.from < 1 ||
                                activePromotion.every < 1
                            }
                            children={activePromotion.id ? 'Сохранить' : 'Добавить'}
                        />
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </div>
    );
}

export default TablePromotions;
