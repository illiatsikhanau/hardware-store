'use client'

import {signOut} from 'next-auth/react';
import {useAppDispatch} from '@/redux/hooks';
import {setFavoriteCount} from '@/redux/userReducer';
import Button from '@/components/Button';

const SignOutButton = () => {
    const dispatch = useAppDispatch();

    const logout = async () => {
        await signOut();
        dispatch(setFavoriteCount(0));
    }

    return (
        <Button onClick={logout} children='Выйти'/>
    );
}

export default SignOutButton;
