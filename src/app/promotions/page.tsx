import {getBanners} from '@/services/banners';
import Banner from '@/components/Banner';

const PromotionsPage = async () => {
    const banners = await getBanners();

    return (
        <div className='flex flex-col w-full gap-4'>
            <h1 className='p-1 text-2xl font-bold' children='Акции'/>
            <div className='flex flex-wrap'>
                {banners.map(banner =>
                    <div key={banner.id} className='flex w-full sm:w-1/2 xl:w-1/3 p-1'>
                        <Banner banner={banner}/>
                    </div>
                )}
            </div>
        </div>
    );
}

export default PromotionsPage;
