import Link from 'next/link';
import {getSubcategories} from '@/services/subcategories';
import {getCategories} from '@/services/categories';
import {getSections} from '@/services/sections';

const sectionStyle = [
    'w-full flex p-2 mb-0.5 max-xs:break-all rounded-md',
    'duration-100 hover:text-white hover:bg-zinc-800'
].join(' ');

const CatalogPage = async ({params}: { params: { categoryId: string[] } }) => {
    const categories = await getCategories();
    const categoryId = params.categoryId ?
        params.categoryId.toString() :
        categories.length ?
            categories[0].id.toString() :
            '0'
    const subcategories = await getSubcategories({categoryId: `equals:${categoryId}`});
    const sections = await getSections();

    return (
        <div className='flex-1 sm:columns-2 lg:columns-3'>
            {subcategories.map(subcategory =>
                <div key={subcategory.id} className='mb-4'>
                    <b className='flex p-2 mb-0.5 max-xs:break-all text-accent' children={subcategory.name}/>
                    {sections.filter(section => section.subcategoryId === subcategory.id).map(section =>
                        <Link
                            key={`${subcategory.id}_${section.id}`}
                            className={sectionStyle}
                            href={`/section/${section.id}`}
                            aria-label={section.name}
                            children={section.name}
                        />
                    )}
                </div>
            )}
        </div>
    );
}

export default CatalogPage;
