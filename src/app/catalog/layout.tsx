import {ReactNode} from 'react';
import {Divider} from '@nextui-org/divider';
import {getCategories} from '@/services/categories';
import CategoryLabel from '@/components/CategoryLabel';
import MenuSelect from '@/components/MenuSelect';

const CatalogLayout = async ({children}: { children: ReactNode }) => {
    const categories = await getCategories();

    return (
        <div className='flex flex-col w-full gap-6'>
            <h1 className='p-1 text-2xl font-bold' children='Каталог'/>
            <div className='flex max-md:flex-col gap-2 p-1'>
                <MenuSelect
                    index='/catalog'
                    items={categories.map(category => {
                        return {
                            name: category.name,
                            href: `/catalog/${category.id}`,
                        }
                    })}
                />
                <aside className='hidden w-fit md:flex flex-col gap-0.5'>
                    {categories.map(category =>
                        <CategoryLabel
                            key={category.id}
                            category={category}
                            index={!categories.indexOf(category)}
                        />
                    )}
                </aside>
                <Divider className='hidden md:flex' orientation='vertical'/>
                {children}
            </div>
        </div>
    );
}

export default CatalogLayout;
