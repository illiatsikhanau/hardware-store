import {ReactNode} from 'react';
import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import {redirect} from 'next/navigation';

const LoginPage = async ({children}: { children: ReactNode }) => {
    const session = await getServerSession(authOptions);
    if (session?.user?.email) redirect('/');

    return (
        <div className='w-full flex flex-col gap-6'>
            <h1 className='p-1 text-2xl font-bold' children='Вход'/>
            {children}
        </div>
    );
}

export default LoginPage;
