'use client';

import {signIn} from 'next-auth/react';
import Button from '@/components/Button';
import {useAppDispatch} from '@/redux/hooks';
import {getProductsInfo} from '@/services/products';
import {setFavoriteCount} from '@/redux/userReducer';

const LoginPage = () => {
    const dispatch = useAppDispatch();

    const googleSignIn = async () => {
        await signIn('google');
        void updateFavoriteCount();
    }

    const yandexSignIn = async () => {
        await signIn('yandex');
        void updateFavoriteCount();
    }

    const updateFavoriteCount = async () => {
        const info = await getProductsInfo({onlyFavorite: true});
        dispatch(setFavoriteCount(info.count));
    }

    return (
        <div className='flex flex-col items-center justify-center gap-3 p-1 pb-16'>
            <Button
                className='shadow-card'
                onClick={googleSignIn}
                children='Войти с помощью Google'
            />
            <Button
                className='shadow-card'
                onClick={yandexSignIn}
                children='Войти с помощью Yandex'
            />
        </div>
    );
}

export default LoginPage;
