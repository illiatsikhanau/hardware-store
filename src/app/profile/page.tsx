import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import {redirect} from 'next/navigation';
import {Role} from '@prisma/client';
import Link from 'next/link';
import Input from '@/components/Input';
import SignOutButton from '@/components/SignOutButton';
import HeartIcon from '@/components/svg/HeartIcon';
import CartIcon from '@/components/svg/CartIcon';
import KeyIcon from '@/components/svg/KeyIcon';

const linkStyle = [
    'flex w-full h-10 items-center gap-2 p-2 rounded-md border-1',
    'border-zinc-200 bg-zinc-100 duration-100 hover:bg-zinc-800 hover:text-white group'
].join(' ');

const linkIconStyle = [
    'flex w-6 h-6 items-center stroke-zinc-700 fill-none',
    'duration-100 group-hover:brightness-0 group-hover:invert'
].join(' ');

const inputStyle = {label: 'w-36', mainWrapper: 'flex flex-1'};

const ProfilePage = async () => {
    const session = await getServerSession(authOptions);
    const email = session?.user?.email;

    if (!email) return redirect('/login');

    return (
        <div className='flex flex-col w-full gap-2'>
            <h1 className='p-1 text-xl font-bold' children='Профиль'/>
            <div className='flex flex-wrap justify-center'>
                <div className='w-full sm:w-1/2 flex flex-col gap-2 p-1'>
                    <Input classNames={inputStyle} label='Бонусные баллы' value='0' readOnly/>
                    <Input classNames={inputStyle} label='Email' defaultValue={email} readOnly/>
                    <Input classNames={inputStyle} label='Имя'/>
                    <Input classNames={inputStyle} label='Телефон'/>
                </div>
                <div className='w-full sm:w-1/2 flex flex-col gap-2 p-1'>
                    {session?.user?.role === Role.ADMIN &&
                        <Link className={linkStyle} href='/admin'>
                            <KeyIcon className={linkIconStyle}/>
                            Управление
                        </Link>
                    }
                    <Link className={linkStyle} href='/favorite'>
                        <HeartIcon className={linkIconStyle}/>
                        Избранное
                    </Link>
                    <Link className={linkStyle} href='/cart'>
                        <CartIcon className={linkIconStyle}/>
                        Корзина
                    </Link>
                    <SignOutButton/>
                </div>
            </div>
        </div>
    );
}

export default ProfilePage;
