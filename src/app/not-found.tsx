import Image from 'next/image';
import notFound from '@/../public/404.jpg';

const NotFoundPage = () => {
    return (
        <div className='h-screen flex flex-col items-center gap-8 px-4 py-32'>
            <Image className='w-full max-w-xl' src={notFound} alt='Изображение'/>
            <h1 className='text-2xl text-accent text-center' children='Уупс, такой страницы не существует'/>
            <a
                className='h-10 flex items-center px-4 bg-accent text-white duration-100 hover:opacity-90'
                href='/'
                aria-label='На главную'
                children='На главную'
            />
        </div>
    );
}

export default NotFoundPage;
