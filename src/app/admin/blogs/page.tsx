'use client'

import {useEffect, useState} from 'react';
import {BlogTypeExt, getBlogTypes} from '@/services/blogTypes';
import TableBlogTypes from '@/components/TableBlogTypes';
import TableBlogs from '@/components/TableBlogs';
import {Tab, Tabs} from '@nextui-org/tabs';
import Spinner from '@/components/Spinner';

const AdminBlogsPage = () => {
    const [blogTypes, setBlogTypes] = useState<BlogTypeExt[]>();
    useEffect(() => {
        getBlogTypes().then(blogTypes => setBlogTypes(blogTypes));
    }, []);

    return (
        <div className='flex flex-col items-center gap-2'>
            {blogTypes ?
                <>
                    <Tabs
                        classNames={{
                            base: 'w-full rounded-md bg-zinc-800',
                            tabList: 'bg-transparent',
                            cursor: 'rounded-md bg-white',
                            tab: 'group data-[hover-unselected=true]:opacity-80',
                            tabContent: 'p-3 text-white group-data-[selected=true]:text-zinc-700'
                        }}
                        aria-label='Статьи'
                    >
                        <Tab className='w-full p-0' title='Статьи'>
                            <TableBlogs blogTypes={blogTypes}/>
                        </Tab>
                        <Tab className='w-full p-0' title='Типы статей'>
                            <TableBlogTypes blogTypes={blogTypes} setBlogTypes={setBlogTypes}/>
                        </Tab>
                    </Tabs>
                </> :
                <Spinner/>
            }
        </div>
    );
}

export default AdminBlogsPage;
