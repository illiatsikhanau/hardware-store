import {ReactNode} from 'react';
import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import {redirect} from 'next/navigation';
import {Role} from '@prisma/client';
import {Divider} from '@nextui-org/divider';
import AdminSectionLabel from '@/components/AdminSectionLabel';
import MenuSelect from '@/components/MenuSelect';

const MENU_ITEMS = [
    {name: 'Инфографика', href: '/admin'},
    {name: 'Пользователи', href: '/admin/users'},
    {name: 'Баннеры', href: '/admin/banners'},
    {name: 'Акции', href: '/admin/promotions'},
    {name: 'Статьи', href: '/admin/blogs'},
    {name: 'Преимущества', href: '/admin/benefits'},
    {name: 'Категории', href: '/admin/categories'},
    {name: 'Подкатегории', href: '/admin/subcategories'},
    {name: 'Подразделы', href: '/admin/sections'},
    {name: 'Товары', href: '/admin/products'},
];

const AdminLayout = async ({children}: { children: ReactNode }) => {
    const session = await getServerSession(authOptions);
    if (session?.user?.role !== Role.ADMIN) redirect('/');

    return (
        <div className='flex flex-col w-full gap-6'>
            <h1 className='p-1 text-2xl font-bold' children='Управление'/>
            <div className='flex max-md:flex-col max-lg:gap-2 lg:gap-4 p-1'>
                <MenuSelect index='/admin' items={MENU_ITEMS}/>
                <aside className='hidden w-fit md:flex flex-col gap-0.5'>
                    {MENU_ITEMS.map(item =>
                        <AdminSectionLabel key={item.name} name={item.name} href={item.href}/>
                    )}
                </aside>
                <Divider className='hidden md:flex' orientation='vertical'/>
                <div className='flex-1' children={children}/>
            </div>
        </div>
    );
}

export default AdminLayout;
