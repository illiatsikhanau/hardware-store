'use client'

import {useEffect, useState, Key, useRef} from 'react';
import {useAppDispatch} from '@/redux/hooks';
import {setActiveSubcategory} from '@/redux/adminReducer';
import {SubcategoryExt, EMPTY_SUBCATEGORY, getSubcategories} from '@/services/subcategories';
import TableSections from '@/components/TableSections';
import {Autocomplete, AutocompleteItem} from '@nextui-org/autocomplete';
import Spinner from '@/components/Spinner';

const AdminSectionsPage = () => {
    const dispatch = useAppDispatch();
    const autocompleteRef = useRef<HTMLInputElement>(null);

    const onSubcategorySelectChange = (key: Key) => {
        const subcategoryId = Number(key);
        const activeSubcategory = subcategories?.find(subcategory => subcategory.id === subcategoryId);
        dispatch(setActiveSubcategory(activeSubcategory ?? EMPTY_SUBCATEGORY));
        autocompleteRef.current?.blur();
    }

    const [subcategories, setSubcategories] = useState<SubcategoryExt[]>();
    useEffect(() => {
        getSubcategories().then(subcategories => setSubcategories(subcategories));
        dispatch(setActiveSubcategory(EMPTY_SUBCATEGORY));
    }, []);

    return (
        <div className='flex flex-col items-center gap-2'>
            {subcategories ?
                <>
                    <Autocomplete
                        inputProps={{classNames: {inputWrapper: 'h-10 rounded-md !bg-zinc-800 text-white'}}}
                        listboxProps={{itemClasses: {base: 'rounded-md bg-zinc-800 text-white'}}}
                        popoverProps={{
                            offset: 10,
                            classNames: {
                                content: 'rounded-md bg-zinc-800 text-white'
                            }
                        }}
                        clearButtonProps={{className: 'text-white'}}
                        selectorButtonProps={{className: 'text-white'}}
                        startContent={<span className='text-sm text-sky-500' children='Подкатегория:'/>}
                        placeholder='Не выбрана'
                        aria-label='Подкатегория'
                        scrollShadowProps={{isEnabled: false}}
                        onSelectionChange={onSubcategorySelectChange}
                        ref={autocompleteRef}
                    >
                        {subcategories.map(subcategory => (
                            <AutocompleteItem
                                key={subcategory.id}
                                value={subcategory.name}
                                children={subcategory.name}
                            />
                        ))}
                    </Autocomplete>
                    <TableSections/>
                </> :
                <Spinner/>
            }
        </div>
    );
}

export default AdminSectionsPage;
