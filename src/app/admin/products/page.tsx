'use client'

import {Key, useEffect, useRef, useState} from 'react';
import {useAppDispatch} from '@/redux/hooks';
import {setActiveSection} from '@/redux/adminReducer';
import {getSections, SectionExt, EMPTY_SECTION} from '@/services/sections';
import {getProductUnits, ProductUnitExt} from '@/services/productUnits';
import TableProductUnits from '@/components/TableProductUnits';
import TableProducts from '@/components/TableProducts';
import {Tab, Tabs} from '@nextui-org/tabs';
import {Autocomplete, AutocompleteItem} from '@nextui-org/autocomplete';
import Spinner from '@/components/Spinner';

const AdminProductsPage = () => {
    const dispatch = useAppDispatch();
    const autocompleteRef = useRef<HTMLInputElement>(null);

    const onSectionSelectChange = (key: Key) => {
        const id = Number(key);
        const activeSection = sections?.find(section => section.id === id);
        dispatch(setActiveSection(activeSection ?? EMPTY_SECTION));
        autocompleteRef.current?.blur();
    }

    const [sections, setSections] = useState<SectionExt[]>();
    useEffect(() => {
        getSections().then(sections => setSections(sections));
    }, []);

    const [productUnits, setProductUnits] = useState<ProductUnitExt[]>();
    useEffect(() => {
        getProductUnits().then(productUnits => setProductUnits(productUnits));
        dispatch(setActiveSection(EMPTY_SECTION));
    }, []);

    return (
        <div className='flex flex-col items-center gap-2'>
            {productUnits ?
                <>
                    <Tabs
                        classNames={{
                            base: 'w-full rounded-md bg-zinc-800',
                            tabList: 'bg-transparent',
                            cursor: 'rounded-md bg-white',
                            tab: 'group data-[hover-unselected=true]:opacity-80',
                            tabContent: 'p-3 text-white group-data-[selected=true]:text-zinc-700'
                        }}
                        aria-label='Товары'
                    >
                        <Tab className='w-full flex flex-col gap-2 p-0' title='Товары'>
                            {sections ?
                                <>
                                    <Autocomplete
                                        inputProps={{
                                            classNames: {
                                                inputWrapper: 'h-10 rounded-md !bg-zinc-800 text-white'
                                            }
                                        }}
                                        listboxProps={{itemClasses: {base: 'rounded-md bg-zinc-800 text-white'}}}
                                        popoverProps={{classNames: {content: 'rounded-md bg-zinc-800 text-white'}}}
                                        clearButtonProps={{className: 'text-white'}}
                                        selectorButtonProps={{className: 'text-white'}}
                                        startContent={<span className='text-sm text-sky-500' children='Подраздел:'/>}
                                        placeholder='Не выбран'
                                        aria-label='Подраздел'
                                        scrollShadowProps={{isEnabled: false}}
                                        onSelectionChange={onSectionSelectChange}
                                        ref={autocompleteRef}
                                    >
                                        {sections.map(section => (
                                            <AutocompleteItem
                                                key={section.id}
                                                value={section.name}
                                                children={section.name}
                                            />
                                        ))}
                                    </Autocomplete>
                                    <TableProducts productUnits={productUnits}/>
                                </> :
                                <Spinner/>
                            }
                        </Tab>
                        <Tab className='w-full p-0' title='Единицы товаров'>
                            <TableProductUnits productUnits={productUnits} setProductUnits={setProductUnits}/>
                        </Tab>
                    </Tabs>
                </> :
                <Spinner/>
            }
        </div>
    );
}

export default AdminProductsPage;
