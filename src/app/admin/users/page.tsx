import {getServerSession} from 'next-auth/next';
import {authOptions} from '@/app/api/auth/[...nextauth]/route';
import TableUsers from '@/components/TableUsers';

const AdminUsersPage = async () => {
    const session = await getServerSession(authOptions);

    return (
        <TableUsers email={session?.user?.email}/>
    );
}

export default AdminUsersPage;
