'use client'

import {useEffect, useState, useRef, Key} from 'react';
import {useAppDispatch} from '@/redux/hooks';
import {setActiveCategory} from '@/redux/adminReducer';
import {CategoryExt, EMPTY_CATEGORY, getCategories} from '@/services/categories';
import TableSubcategories from '@/components/TableSubcategories';
import {Autocomplete, AutocompleteItem} from '@nextui-org/autocomplete';
import Spinner from '@/components/Spinner';

const AdminSubcategoriesPage = () => {
    const dispatch = useAppDispatch();
    const autocompleteRef = useRef<HTMLInputElement>(null);

    const onCategorySelectChange = (key: Key) => {
        const id = Number(key);
        const activeCategory = categories?.find(category => category.id === id);
        dispatch(setActiveCategory(activeCategory ?? EMPTY_CATEGORY));
        autocompleteRef.current?.blur();
    }

    const [categories, setCategories] = useState<CategoryExt[]>();
    useEffect(() => {
        getCategories().then(categories => setCategories(categories));
        dispatch(setActiveCategory(EMPTY_CATEGORY));
    }, []);

    return (
        <div className='flex flex-col items-center gap-2'>
            {categories ?
                <>
                    <Autocomplete
                        inputProps={{classNames: {inputWrapper: 'h-10 rounded-md !bg-zinc-800 text-white'}}}
                        listboxProps={{itemClasses: {base: 'rounded-md bg-zinc-800 text-white'}}}
                        popoverProps={{classNames: {content: 'rounded-md bg-zinc-800 text-white'}}}
                        clearButtonProps={{className: 'text-white'}}
                        selectorButtonProps={{className: 'text-white'}}
                        startContent={<span className='text-sm text-sky-500' children='Категория:'/>}
                        placeholder='Не выбрана'
                        aria-label='Категория'
                        scrollShadowProps={{isEnabled: false}}
                        onSelectionChange={onCategorySelectChange}
                        ref={autocompleteRef}
                    >
                        {categories.map(category => (
                            <AutocompleteItem key={category.id} value={category.name} children={category.name}/>
                        ))}
                    </Autocomplete>
                    <TableSubcategories/>
                </> :
                <Spinner/>
            }
        </div>
    );
}

export default AdminSubcategoriesPage;
