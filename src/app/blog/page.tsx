import {getBlogs} from '@/services/blogs';
import BlogCard from '@/components/BlogCard';

const BlogPage = async () => {
    const blogs = await getBlogs();

    return (
        <div className='flex flex-col w-full gap-4'>
            <h1 className='p-1 text-2xl font-bold' children='Статьи'/>
            <div className='flex flex-wrap'>
                {blogs.map(blog =>
                    <div key={blog.id} className='flex w-full sm:w-1/2 p-1'>
                        <BlogCard blog={blog}/>
                    </div>
                )}
            </div>
        </div>
    );
}

export default BlogPage;
