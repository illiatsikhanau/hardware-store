import Link from 'next/link';

const linkStyle = 'font-bold text-accent duration-100 hover:text-zinc-500';

const AboutPage = () => {
    const customerPhone = '+358-0-000-000';
    const customerEmail = 'hardwarestore@example.com';
    const cooperationPhone = '+358-0-000-000';
    const cooperationEmail = 'hardwarestore@example.com';
    const address = 'Хельсинки, Сенатская площадь';
    const workingHours = 'пн-вс: 8:00-19:00';

    return (
        <div className='flex flex-col w-full gap-6 p-1'>
            <h1 className='text-2xl font-bold' children='О компании'/>
            <p>
                <b>HardwareStore</b> - это финский интернет-магазин, специализирующийся на предоставлении широкого
                ассортимента высококачественных строительных материалов и инструментов. Мы с гордостью являемся вашим
                надежным партнером в создании и реализации проектов любого масштаба, будь то ремонт дома, строительство
                коммерческих объектов или даже профессиональные строительные работы.
            </p>
            <b className='uppercase'>
                Что делает Hardware Store особенным?
            </b>
            <p>
                <b>Качество и надежность:</b> Мы стремимся предлагать только лучшие строительные материалы и инструменты
                от ведущих мировых производителей. Все товары, представленные в нашем магазине, прошли строгий контроль
                качества, чтобы удовлетворить самые высокие стандарты.
            </p>
            <p>
                <b>Большой выбор:</b> Hardware Store предлагает широкий ассортимент товаров для всех видов строительных
                работ. У нас вы найдете всё, от стройматериалов и инструментов до отделочных материалов и сантехники.
                Наш ассортимент постоянно обновляется, чтобы соответствовать последним тенденциям и потребностям наших
                клиентов.
            </p>
            <p>
                <b>Удобство и доступность:</b> Наш интернет-магазин доступен для посещения 24/7, что позволяет вам
                делать покупки в удобное для вас время. Мы также предоставляем оперативную доставку по всей Финляндии,
                чтобы вы могли начать свои строительные проекты как можно скорее.
            </p>
            <p>
                <b>Профессиональная поддержка:</b> Наша команда экспертов всегда готова помочь вам выбрать правильные
                материалы и инструменты, ответить на ваши вопросы и предоставить необходимую консультацию. Мы ценим
                наших клиентов и готовы поддерживать их на каждом этапе.
            </p>
            <div className='flex flex-col gap-4'>
                <p className='flex flex-col'>
                    <b>Телефон для связи с клиентами: </b>
                    <a className={linkStyle} href={`tel:${customerPhone}`}>{customerPhone}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Телефон для сотрудничества: </b>
                    <a className={linkStyle} href={`tel:${cooperationPhone}`}>{cooperationPhone}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Почта для клиентов: </b>
                    <a className={linkStyle} href={`mailto:${customerEmail}`}>{customerEmail}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Почта для сотрудничества: </b>
                    <a className={linkStyle} href={`mailto:${cooperationEmail}`}>{cooperationEmail}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Адрес и время работы: </b>
                    <Link className={linkStyle} href='/'>{address}</Link>
                    <Link className={linkStyle} href='/'>{workingHours}</Link>
                </p>
            </div>
        </div>
    );
}

export default AboutPage;
