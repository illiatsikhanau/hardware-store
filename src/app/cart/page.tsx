'use client'

import {useEffect, useState} from 'react';
import {getProducts, ProductExt} from '@/services/products';
import CartCard from '@/components/CartCard';
import Input from '@/components/Input';
import Button from '@/components/Button';
import Spinner from '@/components/Spinner';

const CartPage = () => {
    const [products, setProducts] = useState<ProductExt[]>();

    useEffect(() => {
        getProducts().then(products => setProducts(products));
    }, []);

    return (
        <div className='flex flex-col w-full gap-6'>
            <h1 className='p-1 text-2xl font-bold' children='Корзина'/>
            {products ?
                <div className='flex max-md:flex-col gap-2'>
                    <div className='flex flex-1 flex-col'>
                        <div className='flex flex-col'>
                            {products.map(product =>
                                <div key={product.id} className='flex w-full p-1'>
                                    <CartCard product={product}/>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className='flex h-fit p-1'>
                        <div
                            className='w-full flex flex-col items-center gap-4 p-4 border-1 border-zinc-200 shadow-card'
                        >
                            <Input placeholder='Номер карты лояльности'/>
                            <div className='w-full flex flex-col s:max-md:flex-row items-center gap-4'>
                                <div className='flex w-full justify-between'>
                                    <b className='text-xl' children='Итого:'/>
                                    <b className='text-xl' children='0€'/>
                                </div>
                                <Button className='w-full' isDisabled children='Оформить заказ'/>
                            </div>
                        </div>
                    </div>
                </div> :
                <Spinner/>
            }
        </div>
    );
}

export default CartPage;
