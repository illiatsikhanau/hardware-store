import {redirect} from 'next/navigation';
import {getSection} from '@/services/sections';
import {getProducts, getProductsInfo, PRODUCTS_PER_PAGE, ProductsQuery} from '@/services/products';
import Breadcrumbs from '@/components/Breadcrumbs';
import FiltersModal from '@/components/FiltersModal';
import SortSelect from '@/components/SortSelect';
import CatalogCard from '@/components/CatalogCard';
import AscSortIcon from '@/components/svg/AscSortIcon';
import DescSortIcon from '@/components/svg/DescSortIcon';
import FiltersList from '@/components/FiltersList';
import ProductsListAppend from '@/components/ProductsListAppend';

const SORT_OPTIONS = [
    {key: 'price:asc', value: 'По цене (по возрастанию)', icon: <AscSortIcon className='w-5 h-5'/>},
    {key: 'price:desc', value: 'По цене (по убыванию)', icon: <DescSortIcon className='w-5 h-5'/>},
    {key: 'name:asc', value: 'По названию (по возрастанию)', icon: <AscSortIcon className='w-5 h-5'/>},
    {key: 'name:desc', value: 'По названию (по убыванию)', icon: <DescSortIcon className='w-5 h-5'/>},
];

interface SectionPageProps {
    params: {
        sectionId: string[]
    }
    searchParams: {
        sort?: string,
        count?: string,
        price?: string,
        sectionId?: string,
    }
}

const SectionPage = async (props: SectionPageProps) => {
    const sectionId = Number(props.params.sectionId);
    const section = await getSection(sectionId);
    if (!section) return redirect('/');

    const sort = props.searchParams.sort ?? SORT_OPTIONS[0].key;
    const count = props.searchParams.count ?? 'gt:0';
    const price = props.searchParams.price ?? undefined;

    const getProductsQuery: ProductsQuery = {
        sort: sort,
        count: count,
        price: price,
        sectionId: `equals:${sectionId}`,
        skip: 0,
        take: PRODUCTS_PER_PAGE,
    }

    const productsInfo = await getProductsInfo({sectionId: getProductsQuery.sectionId});
    const products = await getProducts(getProductsQuery);

    const filtersList = <FiltersList
        minPrice={productsInfo?.minPrice ?? 0}
        maxPrice={productsInfo?.maxPrice ?? 0}
    />

    return (
        <div className='min-h-screen flex flex-col w-full gap-6'>
            <Breadcrumbs
                items={[
                    {href: '/', value: 'Главная'},
                    {href: '/catalog', value: 'Каталог'},
                    {href: `/section/${section.id}`, value: section.name},
                ]}
            />
            <h1 className='p-1 text-2xl font-bold' children={section?.name}/>
            <div className='flex'>
                <aside className='hidden lg:flex flex-col p-1' children={filtersList}/>
                <div className='flex flex-1 flex-col items-center gap-2'>
                    <div className='w-full flex max-s:flex-col-reverse gap-2 p-1 justify-between'>
                        <SortSelect selectedKey={sort} options={SORT_OPTIONS}/>
                        <FiltersModal content={filtersList}/>
                    </div>
                    <div className='w-full flex flex-wrap'>
                        {products.map(product =>
                            <div key={product.id} className='flex w-full sm:w-1/2 xl:w-1/3 p-1'>
                                <CatalogCard product={product}/>
                            </div>
                        )}
                        {products.length === PRODUCTS_PER_PAGE &&
                            <ProductsListAppend query={{
                                ...getProductsQuery,
                                skip: PRODUCTS_PER_PAGE
                            }}/>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SectionPage;
