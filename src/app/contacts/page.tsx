const linkStyle = 'font-bold text-accent duration-100 hover:text-zinc-500';

const ContactsPage = () => {
    const customerPhone = '+358-0-000-000';
    const customerEmail = 'hardwarestore@example.com';
    const cooperationPhone = '+358-0-000-000';
    const cooperationEmail = 'hardwarestore@example.com';
    const address = 'Хельсинки, Сенатская площадь';
    const workingHours = 'пн-вс: 8:00-19:00';

    return (
        <div className='flex flex-col w-full gap-6 p-1'>
            <h1 className='text-2xl font-bold' children='Контакты'/>
            <div className='flex flex-col gap-4'>
                <p className='flex flex-col'>
                    <b>Телефон для связи с клиентами: </b>
                    <a className={linkStyle} href={`tel:${customerPhone}`}>{customerPhone}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Телефон для сотрудничества: </b>
                    <a className={linkStyle} href={`tel:${cooperationPhone}`}>{cooperationPhone}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Почта для клиентов: </b>
                    <a className={linkStyle} href={`mailto:${customerEmail}`}>{customerEmail}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Почта для сотрудничества: </b>
                    <a className={linkStyle} href={`mailto:${cooperationEmail}`}>{cooperationEmail}</a>
                </p>
                <p className='flex flex-col'>
                    <b>Адрес и время работы:</b>
                    <span>{address}, {workingHours}</span>
                </p>
            </div>
            <iframe
                src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1983.6732715680712!2d24.952286999999984
                !3d60.16947899999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!
                1s0x46920bce46c319b7%3A0xbc57e32cb4d908ca!2sSenate%20Square!5e0!3m2!
                1sen!2sby!4v1693946748449!5m2!1sen!2sby'
                title='Расположение'
                width='100%'
                height='500'
                loading='lazy'
                referrerPolicy='no-referrer-when-downgrade'
                allowFullScreen
            />
        </div>
    );
}

export default ContactsPage;
