import './globals.css';
import {ReactNode} from 'react';
import type {Metadata} from 'next';
import {Inter} from 'next/font/google';
import AppProvider from '@/components/AppProvider';
import Header from '@/components/Header';
import Footer from '@/components/Footer';
import ToTopButton from '@/components/ToTopButton';
import TabBar from '@/components/TabBar';

const inter = Inter({subsets: ['latin']});

export const metadata: Metadata = {
    title: 'Hardware Store',
    description: 'Магазин строительных материалов',
}

const bodyStyle = [
    inter.className,
    'flex flex-col text-zinc-700 bg-white'
].join(' ');

const RootLayout = ({children}: { children: ReactNode }) => {
    return (
        <html lang='ru' className='max-xs:text-xs bg-zinc-900' suppressHydrationWarning>
        <body className={bodyStyle}>
        <AppProvider>
            <Header/>
            <main className='w-full max-w-7xl flex justify-center xs:p-3 pb-8 m-auto' children={children}/>
            <Footer/>
            <ToTopButton/>
            <TabBar/>
        </AppProvider>
        </body>
        </html>
    );
}

export default RootLayout;
