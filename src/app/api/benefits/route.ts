import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name, icon, href} = await req.json();
        if (name && icon) {
            const benefit = await prisma.benefit.create({
                data: {
                    name: name,
                    icon: icon,
                    href: href || null,
                }
            });

            return NextResponse.json(benefit);
        } else {
            return badRequest;
        }
    });
}

export const GET = async () => {
    return withApiWrapper([], async () => {
        const benefits = await prisma.benefit.findMany({
            orderBy: [
                {id: 'asc'}
            ]
        });

        return NextResponse.json(benefits);
    });
}
