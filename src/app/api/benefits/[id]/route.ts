import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        const benefits = await prisma.benefit.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(benefits);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name, icon, href} = await req.json();

        if (name && icon) {
            const benefit = await prisma.benefit.update({
                where: {
                    id: id
                },
                data: {
                    name: name,
                    icon: icon,
                    href: href || null,
                }
            });

            return NextResponse.json(benefit);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.benefit.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
