import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';
import {getObjectParamCondition} from '@/helpers/query';
import {BLOGS_PER_PAGE} from '@/services/blogs';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name, src, href, typeId} = await req.json();
        if (name && src && href) {
            const blog = await prisma.blog.create({
                data: {
                    name: name,
                    src: src,
                    href: href,
                    typeId: typeId,
                },
                include: {
                    type: true
                }
            });

            return NextResponse.json(blog);
        } else {
            return badRequest;
        }
    });
}

export const GET = async (req: NextRequest) => {
    return withApiWrapper([], async () => {
        const name = getObjectParamCondition(req, 'name', false);
        const skip = Number(req.nextUrl.searchParams.get('skip')) || 0;
        const take = Number(req.nextUrl.searchParams.get('take')) || BLOGS_PER_PAGE;

        const blogs = await prisma.blog.findMany({
            where: {
                name: name
            },
            orderBy: [
                {createdAt: 'desc'}
            ],
            include: {
                type: true
            },
            skip: skip,
            take: take
        });

        return NextResponse.json(blogs);
    });
}
