import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        const blogs = await prisma.blog.findUnique({
            where: {
                id: id
            },
            include: {
                type: true
            }
        });

        return NextResponse.json(blogs);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name, src, href, typeId} = await req.json();

        if (name && src && href) {
            const blog = await prisma.blog.update({
                where: {
                    id: id
                },
                data: {
                    name: name,
                    src: src,
                    href: href,
                    typeId: typeId,
                },
                include: {
                    type: true
                }
            });

            return NextResponse.json(blog);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.blog.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
