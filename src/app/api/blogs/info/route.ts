import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {getObjectParamCondition} from '@/helpers/query';

export const GET = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const name = getObjectParamCondition(req, 'name', false);

        const info = await prisma.blog.aggregate({
            where: {
                name: name
            },
            _count: true,
        });

        return NextResponse.json({
            count: info._count,
        });
    });
}
