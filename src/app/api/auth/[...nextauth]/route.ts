import prisma from '@/lib/prisma';
import NextAuth from 'next-auth';
import {PrismaAdapter} from '@next-auth/prisma-adapter';
import GoogleProvider from 'next-auth/providers/google';
import YandexProvider from 'next-auth/providers/yandex';

export const authOptions = {
    adapter: PrismaAdapter(prisma),
    pages: {
        signIn: '/login',
    },
    providers: [
        GoogleProvider({
            clientId: process.env.GOOGLE_CLIENT_ID as string,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET as string,
        }),
        YandexProvider({
            clientId: process.env.YANDEX_CLIENT_ID as string,
            clientSecret: process.env.YANDEX_CLIENT_SECRET as string,
        }),
    ],
    callbacks: {
        session({session, user}: { session: any, user: any }) {
            session.user.role = user.role;
            return session;
        },
    },
    theme: {
        colorScheme: 'light' as 'light' | 'auto' | 'dark' | undefined,
        logo: '/logo.svg'
    },
}

const handler = NextAuth(authOptions);

export {handler as GET, handler as POST};
