import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';
import {BANNERS_PER_PAGE} from '@/services/banners';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {src, href, expiresAt} = await req.json();
        if (src) {
            const banner = await prisma.banner.create({
                data: {
                    src: src,
                    href: href || null,
                    expiresAt: expiresAt || null,
                }
            });

            return NextResponse.json(banner);
        } else {
            return badRequest;
        }
    });
}

export const GET = async (req: NextRequest) => {
    return withApiWrapper([], async () => {
        const onlyActive = req.nextUrl.searchParams.get('onlyActive') || false;
        const onlyActiveFilter = {
            OR: [
                {expiresAt: {equals: null}},
                {expiresAt: {gt: new Date()}}
            ]
        }

        const skip = Number(req.nextUrl.searchParams.get('skip')) || 0;
        const take = Number(req.nextUrl.searchParams.get('take')) || BANNERS_PER_PAGE;

        const banners = await prisma.banner.findMany({
            where: onlyActive ? onlyActiveFilter : undefined,
            orderBy: [
                {id: 'desc'}
            ],
            skip: skip,
            take: take
        });

        return NextResponse.json(banners);
    });
}
