import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';

export const GET = async () => {
    return withApiWrapper([Role.ADMIN], async () => {
        const info = await prisma.banner.aggregate({
            _count: true,
        });

        return NextResponse.json({
            count: info._count,
        });
    });
}
