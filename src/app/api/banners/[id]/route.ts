import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        const banners = await prisma.banner.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(banners);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {src, href, expiresAt} = await req.json();

        if (src) {
            const banner = await prisma.banner.update({
                where: {
                    id: id
                },
                data: {
                    src: src,
                    href: href || null,
                    expiresAt: expiresAt || null,
                }
            });

            return NextResponse.json(banner);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.banner.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
