import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {role} = await req.json();

        if (role) {
            const user = await prisma.user.update({
                where: {id: id},
                data: {role: role}
            });

            return NextResponse.json(user);
        } else {
            return badRequest;
        }
    });
}
