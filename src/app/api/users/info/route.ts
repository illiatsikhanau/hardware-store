import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {getObjectParamCondition} from '@/helpers/query';

export const GET = async (req: NextRequest) => {
    return await withApiWrapper([Role.ADMIN], async () => {
        const email = getObjectParamCondition(req, 'email', false);

        const info = await prisma.user.aggregate({
            where: {
                email: email
            },
            _count: true,
        });

        return NextResponse.json({
            count: info._count,
        });
    });
}
