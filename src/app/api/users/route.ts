import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {getObjectParamCondition} from '@/helpers/query';
import {USERS_PER_PAGE} from '@/services/users';

export const GET = async (req: NextRequest) => {
    return await withApiWrapper([Role.ADMIN], async () => {
        const email = getObjectParamCondition(req, 'email', false);
        const skip = Number(req.nextUrl.searchParams.get('skip')) || 0;
        const take = Number(req.nextUrl.searchParams.get('take')) || USERS_PER_PAGE;

        const users = await prisma.user.findMany({
            where: {
                email: email
            },
            orderBy: [
                {id: 'desc'}
            ],
            select: {
                id: true,
                email: true,
                image: true,
                role: true,
            },
            skip: skip,
            take: take
        });

        return NextResponse.json(users);
    });
}
