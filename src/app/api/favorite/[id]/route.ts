import prisma from '@/lib/prisma';
import {NextRequest} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {ok} from '@/helpers/http';

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return await withApiWrapper([], async session => {
        const productId = Number(params.id) || 0;

        await prisma.user.update({
            where: {
                email: session?.user.email
            },
            data: {
                favoriteProducts: {
                    disconnect: {
                        id: productId
                    }
                }
            }
        });

        return ok;
    });
}
