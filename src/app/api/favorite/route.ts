import prisma from '@/lib/prisma';
import {NextRequest} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {ok} from '@/helpers/http';

export const POST = async (req: NextRequest) => {
    return await withApiWrapper([], async session => {
        const {productId} = await req.json();

        await prisma.user.update({
            where: {
                email: session?.user.email
            },
            data: {
                favoriteProducts: {
                    connect: {
                        id: productId
                    }
                }
            }
        });

        return ok;
    });
}
