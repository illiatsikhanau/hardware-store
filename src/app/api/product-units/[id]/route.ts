import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        const productUnits = await prisma.productUnit.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(productUnits);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name} = await req.json();

        if (name) {
            const productUnit = await prisma.productUnit.update({
                where: {
                    id: id
                },
                data: {
                    name: name
                }
            });

            return NextResponse.json(productUnit);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.productUnit.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
