import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name} = await req.json();
        if (name) {
            const productUnit = await prisma.productUnit.create({
                data: {
                    name: name
                }
            });

            return NextResponse.json(productUnit);
        } else {
            return badRequest;
        }
    });
}

export const GET = async () => {
    return withApiWrapper([Role.ADMIN], async () => {
        const productUnits = await prisma.productUnit.findMany({
            orderBy: [
                {id: 'asc'}
            ]
        });

        return NextResponse.json(productUnits);
    });
}
