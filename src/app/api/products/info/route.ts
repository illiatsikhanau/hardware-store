import prisma from '@/lib/prisma';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {getObjectParamCondition} from '@/helpers/query';

export const GET = async (req: NextRequest) => {
    return withApiWrapper([], async session => {
        const onlyFavorite = req.nextUrl.searchParams.get('onlyFavorite') || false;
        const onlyFavoriteFilter = {
            some: {
                email: session?.user.email ?? ''
            }
        }

        const name = getObjectParamCondition(req, 'name', false);
        const count = getObjectParamCondition(req, 'count', true);
        const price = getObjectParamCondition(req, 'price', true);
        const sectionId = getObjectParamCondition(req, 'sectionId', true);

        const info = await prisma.product.aggregate({
            where: {
                favoriteUsers: onlyFavorite ? onlyFavoriteFilter : undefined,
                name: name,
                count: count,
                price: price,
                sectionId: sectionId,
            },
            _count: true,
            _min: {
                price: true
            },
            _max: {
                price: true
            }
        });

        return NextResponse.json({
            count: info._count,
            minPrice: info._min.price,
            maxPrice: info._max.price,
        });
    });
}
