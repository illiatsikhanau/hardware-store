import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([], async () => {
        const id = Number(params.id) || 0;

        const products = await prisma.product.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(products);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name, src, count, price, unitId, sectionId} = await req.json();

        if (src && count >= 0 && price >= 0 && sectionId) {
            const product = await prisma.product.update({
                where: {
                    id: id
                },
                data: {
                    name: name,
                    src: src,
                    count: count,
                    price: price,
                    unitId: unitId,
                    sectionId: sectionId,
                },
                include: {
                    unit: true,
                    promotions: {
                        select: {
                            id: true,
                            chipName: true,
                            chipColor: true,
                        }
                    }
                }
            });

            return NextResponse.json(product);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.product.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
