import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';
import {getObjectParamCondition, getSortCondition} from '@/helpers/query';
import {PRODUCTS_PER_PAGE} from '@/services/products';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name, src, count, price, unitId, sectionId} = await req.json();
        if (name && src && count >= 0 && price >= 0 && sectionId) {
            const product = await prisma.product.create({
                data: {
                    name: name,
                    src: src,
                    count: count,
                    price: price,
                    unitId: unitId,
                    sectionId: sectionId,
                },
                include: {
                    unit: true,
                    promotions: {
                        select: {
                            id: true,
                            chipName: true,
                            chipColor: true,
                        }
                    }
                }
            });

            return NextResponse.json(product);
        } else {
            return badRequest;
        }
    });
}

export const GET = async (req: NextRequest) => {
    return withApiWrapper([], async session => {
        const email = session?.user.email ?? '';

        const onlyFavorite = req.nextUrl.searchParams.get('onlyFavorite') || false;
        const onlyFavoriteFilter = {
            some: {
                email: email
            }
        }

        const sort = getSortCondition(req);
        const name = getObjectParamCondition(req, 'name', false);
        const count = getObjectParamCondition(req, 'count', true);
        const price = getObjectParamCondition(req, 'price', true);
        const sectionId = getObjectParamCondition(req, 'sectionId', true);
        const skip = Number(req.nextUrl.searchParams.get('skip')) || 0;
        const take = Number(req.nextUrl.searchParams.get('take')) || PRODUCTS_PER_PAGE;

        const products = await prisma.product.findMany({
            where: {
                favoriteUsers: onlyFavorite ? onlyFavoriteFilter : undefined,
                name: name,
                count: count,
                price: price,
                sectionId: sectionId,
            },
            include: {
                unit: true,
                promotions: {
                    select: {
                        id: true,
                        chipName: true,
                        chipColor: true,
                    }
                },
                favoriteUsers: {
                    where: {
                        email: email
                    },
                    select: {
                        emailVerified: true
                    }
                }
            },
            orderBy: sort,
            skip: skip,
            take: take
        });

        const isFavoriteProducts = products.map(
            ({favoriteUsers: isFavorite, ...product}) => ({isFavorite: isFavorite.length > 0, ...product})
        );

        return NextResponse.json(isFavoriteProducts);
    });
}
