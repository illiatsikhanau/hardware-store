import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([], async () => {
        const id = Number(params.id) || 0;

        const subcategories = await prisma.subcategory.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(subcategories);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name, categoryId} = await req.json();

        if (name && categoryId) {
            const subcategory = await prisma.subcategory.update({
                where: {
                    id: id
                },
                data: {
                    name: name,
                    categoryId: categoryId,
                }
            });

            return NextResponse.json(subcategory);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.subcategory.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
