import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';
import {getObjectParamCondition} from '@/helpers/query';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name, categoryId} = await req.json();
        if (name && categoryId) {
            const subcategory = await prisma.subcategory.create({
                data: {
                    name: name,
                    categoryId: categoryId,
                }
            });

            return NextResponse.json(subcategory);
        } else {
            return badRequest;
        }
    });
}

export const GET = async (req: NextRequest) => {
    return withApiWrapper([], async () => {
        const categoryId = getObjectParamCondition(req, 'categoryId', true);

        const subcategories = await prisma.subcategory.findMany({
            where: {
                categoryId: categoryId,
            },
            orderBy: [
                {name: 'asc'}
            ]
        });

        return NextResponse.json(subcategories);
    });
}
