import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        const blogTypes = await prisma.blogType.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(blogTypes);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name} = await req.json();

        if (name) {
            const blogType = await prisma.blogType.update({
                where: {
                    id: id
                },
                data: {
                    name: name
                }
            });

            return NextResponse.json(blogType);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.blogType.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
