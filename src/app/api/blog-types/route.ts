import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name} = await req.json();
        if (name) {
            const blogType = await prisma.blogType.create({
                data: {
                    name: name
                }
            });

            return NextResponse.json(blogType);
        } else {
            return badRequest;
        }
    });
}

export const GET = async () => {
    return withApiWrapper([Role.ADMIN], async () => {
        const blogTypes = await prisma.blogType.findMany({
            orderBy: [
                {id: 'asc'}
            ]
        });

        return NextResponse.json(blogTypes);
    });
}
