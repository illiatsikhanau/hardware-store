import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        const promotions = await prisma.promotion.findUnique({
            where: {
                id: id
            },
            include: {
                products: {
                    select: {
                        id: true,
                    }
                }
            }
        });

        return NextResponse.json(promotions);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name, chipName, chipColor, discount, from, every, startsAt, expiresAt, products} = await req.json();

        if (name && chipName && chipColor >= 0 && discount >= 0 && from >= 1 && every >= 1 && products) {
            const promotion = await prisma.promotion.update({
                where: {
                    id: id
                },
                include: {
                    products: {
                        select: {
                            id: true,
                        }
                    }
                },
                data: {
                    name: name,
                    chipName: chipName,
                    chipColor: chipColor,
                    discount: discount,
                    from: from,
                    every: every,
                    startsAt: startsAt,
                    expiresAt: expiresAt,
                    products: {
                        set: products
                    },
                },
            });

            return NextResponse.json(promotion);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.promotion.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
