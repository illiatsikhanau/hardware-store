import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';
import {getObjectParamCondition} from '@/helpers/query';
import {PROMOTIONS_PER_PAGE} from '@/services/promotions';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name, chipName, chipColor, discount, from, every, startsAt, expiresAt, products} = await req.json();
        if (name && chipName && chipColor >= 0 && discount >= 0 && from >= 1 && every >= 1 && products) {
            let promotion = await prisma.promotion.create({
                data: {
                    name: name,
                    chipName: chipName,
                    chipColor: chipColor,
                    discount: discount,
                    from: from,
                    every: every,
                    startsAt: startsAt,
                    expiresAt: expiresAt,
                }
            });

            promotion = await prisma.promotion.update({
                where: {
                    id: promotion.id
                },
                include: {
                    products: {
                        select: {
                            id: true,
                        }
                    }
                },
                data: {
                    products: {
                        set: products
                    },
                },
            });

            return NextResponse.json(promotion);
        } else {
            return badRequest;
        }
    });
}

export const GET = async (req: NextRequest) => {
    return withApiWrapper([], async () => {
        const name = getObjectParamCondition(req, 'name', false);
        const skip = Number(req.nextUrl.searchParams.get('skip')) || 0;
        const take = Number(req.nextUrl.searchParams.get('take')) || PROMOTIONS_PER_PAGE;

        const promotions = await prisma.promotion.findMany({
            where: {
                name: name,
            },
            orderBy: [
                {id: 'desc'}
            ],
            include: {
                products: {
                    select: {
                        id: true,
                    }
                }
            },
            skip: skip,
            take: take
        });

        return NextResponse.json(promotions);
    });
}
