import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, serviceUnavailable} from '@/helpers/http';

const UPLOAD_PRESET = process.env.CLOUDINARY_UPLOAD_PRESET as string;
const CLOUD_NAME = process.env.CLOUDINARY_CLOUD_NAME as string;
const FOLDER = process.env.CLOUDINARY_FOLDER as string;

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const formData = await req.formData();
        if (formData && formData.has('file')) {
            formData.append('upload_preset', UPLOAD_PRESET);
            formData.append('cloud_name', CLOUD_NAME);
            formData.append('folder', FOLDER);

            const result = await fetch(`https://api.cloudinary.com/v1_1/${CLOUD_NAME}/image/upload`, {
                method: 'POST',
                body: formData,
            });

            if (result.ok) {
                const data = await result.json();
                const url = data['secure_url'];
                if (url) {
                    return NextResponse.json(url);
                } else {
                    return serviceUnavailable;
                }
            } else {
                return NextResponse.json(result.body, {status: result.status});
            }
        } else {
            return badRequest;
        }
    });
}
