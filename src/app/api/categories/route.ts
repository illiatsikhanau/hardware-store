import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name, src, icon} = await req.json();
        if (name && src && icon) {
            const category = await prisma.category.create({
                data: {
                    name: name,
                    src: src,
                    icon: icon,
                }
            });

            return NextResponse.json(category);
        } else {
            return badRequest;
        }
    });
}

export const GET = async () => {
    return withApiWrapper([], async () => {
        const categories = await prisma.category.findMany({
            orderBy: [
                {id: 'asc'}
            ]
        });

        return NextResponse.json(categories);
    });
}
