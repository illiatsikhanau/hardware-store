import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([], async () => {
        const id = Number(params.id) || 0;

        const categories = await prisma.category.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(categories);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name, src, icon} = await req.json();

        if (name && src && icon) {
            const category = await prisma.category.update({
                where: {
                    id: id
                },
                data: {
                    name: name,
                    src: src,
                    icon: icon,
                }
            });

            return NextResponse.json(category);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.category.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
