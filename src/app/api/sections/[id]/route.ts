import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest, ok} from '@/helpers/http';

export const GET = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([], async () => {
        const id = Number(params.id) || 0;

        const sections = await prisma.section.findUnique({
            where: {
                id: id
            }
        });

        return NextResponse.json(sections);
    });
}

export const PUT = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;
        const {name, subcategoryId} = await req.json();

        if (name && subcategoryId) {
            const section = await prisma.section.update({
                where: {
                    id: id
                },
                data: {
                    name: name,
                    subcategoryId: subcategoryId,
                }
            });

            return NextResponse.json(section);
        } else {
            return badRequest;
        }
    });
}

export const DELETE = async (req: NextRequest, {params}: { params: { id: string } }) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const id = Number(params.id) || 0;

        await prisma.section.delete({
            where: {
                id: id
            }
        });

        return ok;
    });
}
