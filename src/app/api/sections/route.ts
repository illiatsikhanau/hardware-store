import prisma from '@/lib/prisma';
import {Role} from '@prisma/client';
import {NextRequest, NextResponse} from 'next/server';
import {withApiWrapper} from '@/helpers/apiWrapper';
import {badRequest} from '@/helpers/http';
import {getObjectParamCondition} from '@/helpers/query';

export const POST = async (req: NextRequest) => {
    return withApiWrapper([Role.ADMIN], async () => {
        const {name, subcategoryId} = await req.json();
        if (name && subcategoryId) {
            const section = await prisma.section.create({
                data: {
                    name: name,
                    subcategoryId: subcategoryId
                }
            });

            return NextResponse.json(section);
        } else {
            return badRequest;
        }
    });
}

export const GET = async (req: NextRequest) => {
    return withApiWrapper([], async () => {
        const subcategoryId = getObjectParamCondition(req, 'subcategoryId', true);

        const sections = await prisma.section.findMany({
            where: {
                subcategoryId: subcategoryId
            },
            orderBy: [
                {name: 'asc'}
            ]
        });

        return NextResponse.json(sections);
    });
}
