'use client'

import {useEffect, useState} from 'react';
import {getProducts, ProductExt} from '@/services/products';
import CatalogCard from '@/components/CatalogCard';
import Spinner from '@/components/Spinner';

const FavoritePage = () => {
    const [products, setProducts] = useState<ProductExt[]>();

    useEffect(() => {
        getProducts({onlyFavorite: true}).then(products => setProducts(products));
    }, []);

    return (
        <div className='min-h-screen flex flex-col w-full gap-6'>
            <h1 className='p-1 text-2xl font-bold' children='Избранное'/>
            {products ?
                <div className='flex flex-wrap'>
                    {products.length ?
                        products.map(product =>
                            <div key={product.id} className='flex w-full sm:w-1/2 lg:w-1/3 xl:w-1/4 p-1'>
                                <CatalogCard product={product}/>
                            </div>
                        ) :
                        <b
                            className='w-full p-2 m-1 text-lg text-center border-1 border-dashed border-zinc-300 text-zinc-500'
                            children='Список избранного пуст'
                        />
                    }
                </div> :
                <Spinner/>
            }
        </div>
    );
}

export default FavoritePage;
