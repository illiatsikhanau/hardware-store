import BannerSwiper from '@/components/BannerSwiper';
import ProductSwiper from '@/components/ProductSwiper';
import CategoryCard from '@/components/CategoryCard';
import BenefitCard from '@/components/BenefitCard';
import CatalogCard from '@/components/CatalogCard';
import BlogCard from '@/components/BlogCard';
import {getBlogs} from '@/services/blogs';
import {getBanners} from '@/services/banners';
import {getBenefits} from '@/services/benefits';
import {getCategories} from '@/services/categories';
import {getProducts} from '@/services/products';
import Link from 'next/link';

const HomePage = async () => {
    const banners = await getBanners({onlyActive: true});
    const categories = await getCategories();
    const benefits = await getBenefits();
    const products = await getProducts({take: 12});
    const blogs = await getBlogs();

    return (
        <div className='flex w-full flex-col gap-4'>
            <h1 className='hidden' children='Hardware Store - магазин строительных материалов'/>
            <BannerSwiper banners={banners}/>
            <div className='flex w-full flex-wrap'>
                {categories.map(category =>
                    <div
                        key={category.id}
                        className='flex w-full xs:w-1/2 sm:w-1/3 md:w-1/4 lg:w-1/6 justify-center p-1'
                    >
                        <CategoryCard category={category}/>
                    </div>
                )}
            </div>
            <div className='flex w-full flex-wrap'>
                {benefits.map(benefit =>
                    <div key={benefit.id} className='flex w-full md:w-1/2 xl:w-1/4 p-1'>
                        <BenefitCard benefit={benefit}/>
                    </div>
                )}
            </div>
            <section className='flex w-full flex-col items-center'>
                <h2 className='text-2xl font-bold text-center p-1' children='Популярные товары'/>
                <Link
                    className='p-1 text-lg text-zinc-500 text-center duration-100 hover:text-zinc-700'
                    href='/catalog'
                    children='смотреть все'
                />
                <div className='hidden w-full lg:flex flex-wrap'>
                    {products.map(product =>
                        <div key={product.id} className='flex w-1/3 xl:w-1/4 p-1'>
                            <CatalogCard product={product}/>
                        </div>
                    )}
                </div>
                <ProductSwiper products={products}/>
            </section>
            <section className='flex w-full flex-col items-center'>
                <h2 className='text-2xl font-bold text-center p-1' children='Статьи'/>
                <Link
                    className='p-1 text-lg text-zinc-500 text-center duration-100 hover:text-zinc-700'
                    href='/blog'
                    children='все статьи'
                />
                <div className='flex w-full flex-wrap'>
                    {blogs.map(blog =>
                        <div key={blog.id} className='flex w-full lg:w-1/2 p-1'>
                            <BlogCard blog={blog}/>
                        </div>
                    )}
                </div>
            </section>
        </div>
    );
}

export default HomePage;
