const onScroll = () => {
    const sticky = document.getElementById('sticky');
    const toTopButton = document.getElementById('to-top-button');
    if (window.scrollY > 160) {
        sticky.classList.add('fixed');
        sticky.classList.add('bg-white/70');
        toTopButton.classList.remove('hidden');
    } else {
        sticky.classList.remove('fixed');
        sticky.classList.remove('bg-white/70');
        toTopButton.classList.add('hidden');
    }
}

// sticky header and toTopButton visibility
window.addEventListener('scroll', onScroll);
onScroll();
